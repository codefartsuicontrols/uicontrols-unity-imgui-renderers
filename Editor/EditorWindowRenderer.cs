﻿namespace Codefarts.UIControls.Unity_Legacy_Renderers
{
    using System;

    using Codefarts.UIControls.Editor;
    using Codefarts.UIControls.Renderers;

    using UnityEngine;

    [ControlRenderer(typeof(EditorWindow))]
    public class EditorWindowRenderer : ScrollViewerRenderer
    {
        public const string SpecialWindowReference = "SpecialWindowReference_18966BC4-2AF9-4221-A3F7-56E0811B5BE3";

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args"></param>
        /// <exception cref="System.ArgumentNullException">'control' argument is null.</exception>
        public override void DrawControl(ControlRenderingArgs args)
        {
            var window = args.Control as EditorWindow;
            if (window == null)
            {
                throw new InvalidCastException("Expected a EditorWindow control type.");
            }

            SpecialEditorWindow special;
            object objectRef;
            if (window.Properties.TryGetValue(SpecialWindowReference, out objectRef) && objectRef is SpecialEditorWindow)
            {
                special = (SpecialEditorWindow)objectRef;
                window.Location = special.position.min;
                window.Size = special.position.size;
                special.titleContent = new GUIContent(window.Title);
                var size = window.Size;
                if (Math.Abs(size.Width - special.position.size.x) > float.Epsilon ||
                    Math.Abs(size.Height - special.position.size.y) > float.Epsilon)
                {
                    special.position = new Rect(special.position.position, size);
                }
            }

            //args.Offset = new Point(-window.Left, -window.Top);
            // var rect = new Rect(-window.Left, -window.Top, window.Left + window.Width, window.Top + window.Height);
            // using (var area = new GUI.GroupScope(rect))
            // {
            base.DrawControl(args);
            // }
        }    

        #region Overrides of BaseRenderer

        /// <summary>
        /// Updates the specified control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        /// <remarks>
        /// <p>This method is provided if a control has is animated and it's animation state can be updated independently of drawing.</p>
        /// <p>Updates generally occur more frequently then draws.</p>
        /// <p>This base renderer implementation will throw exceptions if the provided control is null or not of the expected type. </p>
        /// </remarks>
        public override void Update(ControlRenderingArgs args)
        {
            var window = args.Control as EditorWindow;
            if (window == null)
            {
                throw new InvalidCastException("Expected a EditorWindow control type.");
            }

            SpecialEditorWindow special;
            object objectRef;
            if (window.Properties.TryGetValue(SpecialWindowReference, out objectRef) && objectRef is SpecialEditorWindow)
            {
                special = (SpecialEditorWindow)objectRef;
                if (BaseRenderer.NeedsRefresh)
                {
                    BaseRenderer.DropRefresh();
                    special.Repaint();
                }
            }

            base.Update(args);
        }

        #endregion
    }
}
