namespace Codefarts.UIControls.Unity_Legacy_Renderers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
                                        
   // using Codefarts.Input.Unity;
    using Codefarts.UIControls.Factories;

    using UnityEngine;

    using EditorWindow = Codefarts.UIControls.Editor.EditorWindow;

    /// <summary>
    /// Provides a resuable unity editor window for controls that inherit from <see cref="UIControls.Editor.EditorWindow"/>.
    /// </summary>
    public class SpecialEditorWindow : UnityEditor.EditorWindow
    {
        /// <summary>
        /// The windows dictionary used to hold caches of windows to prevent duplicate windows from being created.
        /// </summary>
        private static Dictionary<string, EditorWindow> windows;

        /// <summary>
        /// The control manager reference for managing control focus and user input.
        /// </summary>
       // private ControlManager controlManager;

        private Control screenControl;

        /// <summary>
        /// Initializes the <see cref="SpecialEditorWindow"/> class.
        /// </summary>
        static SpecialEditorWindow()
        {
            windows = new Dictionary<string, EditorWindow>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecialEditorWindow"/> class.
        /// </summary>
        public SpecialEditorWindow()
        {
            this.screenControl = new Control() { Size = new Size(int.MaxValue, int.MaxValue) };
            //this.controlManager = new ControlManager();
            //this.controlManager.Control = this.screenControl;
            //var keyboardDevice = new UnityEventKeyboardDevice();
            //this.controlManager.InputManger.AddDevice(keyboardDevice);
            //var mouseDevice = new UnityEventMouseDevice();
            //this.controlManager.InputManger.AddDevice(mouseDevice);
            //this.controlManager.InputManger.Bind("MouseX", mouseDevice.Name, "X");
            //this.controlManager.InputManger.Bind("MouseY", mouseDevice.Name, "Y");
            //this.controlManager.InputManger.Bind("MouseLeft", mouseDevice.Name, "Left");
            //this.controlManager.InputManger.Bind("MouseRight", mouseDevice.Name, "Right");
            //this.controlManager.InputManger.Bind("MouseMiddle", mouseDevice.Name, "Middle");
            //var keySources = keyboardDevice.Sources;
            //foreach (var keySource in keySources)
            //{
            //    this.controlManager.InputManger.Bind(keySource, keyboardDevice.Name, keySource);
            //}
        }

        /// <summary>
        /// The control value for the <see cref="Control"/> property.
        /// </summary>
        private EditorWindow control;

        /// <summary>
        /// The type containing the <see cref="Type.FullName"/> of the assigned <see cref="Control"/>.
        /// </summary>
        /// <remarks>The types full name is stored in this varible so that it is preserved if unity recompiles scripts.</remarks>
        [SerializeField]
        private string type;

        private Control lastMouseControl;

        private bool mouseEntered = false;

        //  private MouseEventArgs mouseArguments = new MouseEventArgs();

        private Vector2 previousMousePosition;

        //  private Vector3 lastMousePos;

        /// <summary>
        /// Gets or sets a reference to a <see cref="EditorWindow"/> control that the <see cref="SpecialEditorWindow"/> is intended to emulate.
        /// </summary>
        public EditorWindow Control
        {
            get
            {
                return this.control;
            }

            set
            {
                // be sure to remove event hooks 
                if (this.control != null)
                {
                    this.control.Shown -= this.ShowTheWindow;
                    this.control.Closed -= this.CloseTheWindow;
                }

                // set the new control value
                this.control = value;

                this.screenControl.Controls.Clear();

                // add event hooks if a value was specified
                if (this.control != null)
                {
                    this.screenControl.Controls.Add(this.control);
                    if (this.control.MinimumSize != Size.Empty)
                    {
                        this.minSize = this.control.MinimumSize;
                    }

                    if (this.control.MaximumSize != Size.Empty)
                    {
                        this.maxSize = this.control.MaximumSize;
                    }

                    this.control.Size = this.position.size;
                    // save the full name of the control being assigned
                    this.type = this.control.GetType().FullName;
                    this.control.Shown += this.ShowTheWindow;
                    this.control.Closed += this.CloseTheWindow;
                }
                else
                {
                    // clear the control type
                    this.type = null;
                }
            }
        }

        /// <summary>
        /// Handles the <see cref="Window.Closed"/> event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void CloseTheWindow(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Handles the <see cref="Window.Shown"/> event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ShowTheWindow(object sender, EventArgs e)
        {
            this.Show();
        }

        /// <summary>
        /// This function is called when the MonoBehaviour will be destroyed.
        /// </summary>
        public void OnDestroy()
        {
            // store a reference to the control then set it to null
            var temp = this.control;
            this.Control = null;

            // if there was a previous reference close it now that event hooks have been removed and
            // remove it from the windows dictionary
            if (temp != null)
            {
                temp.Close();
                windows.Remove(temp.GetType().FullName);
            }
        }

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        public void OnEnable()
        {
            // check if the control field has been set and if the type field has a value
            if (this.control == null && this.type != null)
            {
                // if we have made it here then unity may have recompiled scripts resulting the the reference to 
                // control field to be lost as it can not be serialized. We call Get here to recreate the control
                var win = Get(this.type, this);
                win.Show(null);
            }

            this.autoRepaintOnSceneChange = true;
         //   this.controlManager.InputEnabled = true;
        }

        /// <summary>
        /// This function is called when the behaviour becomes disabled or inactive.
        /// </summary>
        public void OnDisable()
        {
         //   this.controlManager.InputEnabled = false;
        }

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        public void OnGUI()
        {
            var editorWindow = this.control;
            if (editorWindow == null)
            {
                return;
            }

            //  var rect = new Rect(-editorWindow.Left, -editorWindow.Top, editorWindow.Left + editorWindow.Width, editorWindow.Top + editorWindow.Height);
            var rect = new Rect(-editorWindow.Left, -editorWindow.Top, editorWindow.Left + editorWindow.Width, editorWindow.Top + editorWindow.Height);
            using (var area = new GUI.GroupScope(rect))
            {
              //  this.controlManager.Update();

                //  var originalDepth = GUI.depth;
                // GUI.depth = int.MaxValue;
                UIControlsFactory.DefaultManager.Renderer.DrawControl(editorWindow, Time.deltaTime, Time.time);
                // GUI.depth = originalDepth;
                //  this.HandleMouseEvents(editorWindow, editorWindow.Location);
            }

            // this.Repaint();                                                                          

          //  SelectionManager.Instance.Clear();
            //SelectionManager.Instance.Add(editorWindow);
            var mousePosition = Event.current.mousePosition;// + editorWindow.Location;
            var child = editorWindow.FindControlAtPoint(mousePosition);
            if (child == null && mousePosition.x >= 0 && mousePosition.y >= 0 && mousePosition.x < editorWindow.Width && mousePosition.y < editorWindow.Height)
            {
                child = editorWindow;
            }
          //  SelectionManager.Instance.Add(this.GetHeiarchyData(child) + "\r\nChild: " + child + "\r\nMouse: " + mousePosition);

            //if (BaseRenderer.NeedsRefresh)
            //{
            //    BaseRenderer.DropRefresh();
            //    this.Repaint();
            //}

        }

        /*

        /// <summary>
        /// Handles the <see cref="Control.MouseEnter"/>, <see cref="Control.MouseLeave"/> & <see cref="Control.MouseMove"/> events.
        /// </summary>
        /// <param name="control">The control to check.</param>
        protected void HandleMouseEvents(Control control, Vector2 offset)
        {
            var current = Event.current;
            var mousePos = current.mousePosition;


            var leftButton = current.button == 0;
            var rightButton = current.button == 1;
            var middleButton = current.button == 2;
            var button1 = current.button == 3;
            var button2 = current.button == 4;

            var relativeMousePosition = offset + control.PointToClient(mousePos) - control.Location;

            this.mouseArguments.Buttons[Constants.LeftMouseButton] = leftButton ? 1 : 0;
            this.mouseArguments.Buttons[Constants.RightMouseButton] = rightButton ? 1 : 0;
            this.mouseArguments.Buttons[Constants.MiddleMouseButton] = middleButton ? 1 : 0;
            this.mouseArguments.Buttons[Constants.MouseButton4] = button1 ? 1 : 0;
            this.mouseArguments.Buttons[Constants.MouseButton5] = button2 ? 1 : 0;
            this.mouseArguments.X = relativeMousePosition.x;
            this.mouseArguments.Y = relativeMousePosition.y;

            var props = control.Properties;

            var callback = new Action<MouseEventArgs, Control, Vector2>((arg, ctrl, mPos) =>
                 {
                     var rel = mPos - ctrl.Location;
                     arg.X = Math.Max(0, rel.x);
                     arg.Y = Math.Max(0, rel.y);
                     arg.X = Math.Min(ctrl.Width, arg.X);
                     arg.Y = Math.Min(ctrl.Height, arg.Y);
                 });

            if (mousePos != this.previousMousePosition)
            {
                var ctrl = control.FindControlAtPoint(mousePos);
                if (ctrl != this.lastMouseControl)
                {
                    if (this.lastMouseControl != null && this.mouseEntered)
                    {
                        callback(this.mouseArguments, this.lastMouseControl, mousePos);
                        this.mouseArguments.Type = MouseEventType.MouseLeave;
                        this.lastMouseControl.OnMouseEvent(this.mouseArguments);
                        this.lastMouseControl = null;
                        this.mouseEntered = false;
                        props[UIControls.Control.IsMouseOverKey] = true;
                    }

                    if (ctrl != null && this.mouseEntered == false)
                    {
                        callback(this.mouseArguments, ctrl, mousePos);
                        this.mouseArguments.Type = MouseEventType.MouseEnter;
                        ctrl.OnMouseEvent(this.mouseArguments);
                        this.mouseEntered = true;
                        this.lastMouseControl = ctrl;
                        props[UIControls.Control.IsMouseOverKey] = false;
                    }
                }
                else
                {
                    if (ctrl != null)
                    {
                        callback(this.mouseArguments, ctrl, mousePos);
                        this.mouseArguments.Type = MouseEventType.MouseMove;
                        ctrl.OnMouseEvent(this.mouseArguments);
                    }
                }

                this.previousMousePosition = mousePos;
            }
        }
           
                  private string GetControlHeiarchyData(Control control)
        {
            if (control == null)
            {
                return string.Empty;
            }

            var data = string.Empty;
            var mPos = Event.current.mousePosition;
            //  mPos.y = -mPos.y;
            var mousePosition = control.PointToScreen(mPos);
            Point point;
            while (control != null)
            {
                point = control.PointToClient(mPos);
                // if (point.X >= control.Left && point.Y >= control.Top && point.X < control.Left + control.Width && point.Y < control.Top + control.Height)
                if (point.X >= 0 && point.Y >= 0 && point.X < control.Width && point.Y < control.Height)
                {
                    data += string.Format("{0} Location:{1} Size:{2}\r\n", control, control.Location, control.Size);
                }

                control = control.Parent;
            }

            return data;
        }
            */

        private string GetHeiarchyData(Control control)
        {
            if (control == null)
            {
                return string.Empty;
            }

            var data = string.Empty;
            while (control != null)
            {
                data += string.Format("{0} Location:{1} Size:{2} Visible: {3}\r\n", control, control.Location, control.Size, control.IsVisible);
                control = control.Parent;
            }

            return data;
        }



        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        public void Update()
        {
            var editorWindow = this.control;
            if (editorWindow == null)
            {
                return;
            }

            //if (this.lastMousePos != Input.mousePosition)
            //{
            //    this.lastMousePos = Input.mousePosition;
            //    BaseRenderer.QueueRefresh();              
            //    Debug.Log("refresh queued");
            //}

            var originalDepth = GUI.depth;
            GUI.depth = int.MaxValue;
            UIControlsFactory.DefaultManager.Renderer.UpdateControl(editorWindow, Time.deltaTime, Time.time);
            GUI.depth = originalDepth;
        }

        /// <summary>
        /// Gets or creates the specified <see cref="EditorWindow" /> type.
        /// </summary>
        /// <typeparam name="T">The window type to be retrieved.</typeparam>
        /// <returns>
        /// A reference to a existing or newly created <see cref="EditorWindow" /> implementation.
        /// </returns>
        public static T Get<T>() where T : EditorWindow, new()
        {
            return (T)Get(typeof(T).FullName);
        }

        /// <summary>
        /// Gets or creates the specified <see cref="EditorWindow"/> type.
        /// </summary>
        /// <param name="type">The type name of the <see cref="EditorWindow"/> implementation to be retrieved.</param>
        /// <returns>A reference to a existing or newly created <see cref="EditorWindow"/> implementation.</returns>
        public static EditorWindow Get(string type)
        {
            return Get(type, null);
        }

        /// <summary>
        /// Gets or creates the specified <see cref="EditorWindow"/> type.
        /// </summary>
        /// <param name="type">The type name of the <see cref="EditorWindow"/> implementation to be retrieved.</param>
        /// <param name="existing">A reference to an existing <see cref="SpecialEditorWindow"/></param>
        /// <returns>A reference to a existing or newly created <see cref="EditorWindow"/> implementation.</returns>
        internal static EditorWindow Get(string type, SpecialEditorWindow existing)
        {
            EditorWindow win;
            if (!windows.TryGetValue(type, out win))
            {
                var windowType = Type.GetType(type);
                if (windowType != null)
                {
                    win = (EditorWindow)windowType.Assembly.CreateInstance(type);
                    var special = existing != null ? existing : (SpecialEditorWindow)CreateInstance(typeof(SpecialEditorWindow));
                    special.Control = win;
                    win.Properties[EditorWindowRenderer.SpecialWindowReference] = special;
                    windows.Add(windowType.FullName, win);
                }
            }

            return win;
        }

        /// <summary>
        /// Gets the windows that are currently being cached for reuse.
        /// </summary>
        /// <returns>An array of <see cref="EditorWindow"/> references.</returns>
        public static EditorWindow[] GetWindows()
        {
            return windows.Select(x => x.Value).ToArray();
        }

        /// <summary>
        /// Shows the desired window based on a specified type.
        /// </summary>
        /// <typeparam name="T">The window type that inherits from <see cref="EditorWindow"/>.</typeparam>
        public static T ShowWindow<T>() where T : EditorWindow, new()
        {
            try
            {
                var win = Get(typeof(T).FullName);
                if (win != null)
                {
                    win.Show(null);
                }
                return (T)win;
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }

            return null;
        }
    }
}