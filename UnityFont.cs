namespace Codefarts.UIControls
{
    public class UnityFont : Font
    {
        public UnityEngine.Font UnityFontReference { get; set; }

        public UnityFont(string fontName, float size) : base(fontName, size)
        {
            this.UnityFontReference = UnityEngine.Font.CreateDynamicFontFromOSFont(fontName, (int)size);
        }
    }
}