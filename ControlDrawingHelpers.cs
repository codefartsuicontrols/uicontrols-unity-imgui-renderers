/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.UIControls
{
    using System;
    using Codefarts.UIControls.Unity;

    using UnityEngine;

    using Vector2 = UnityEngine.Vector2;

    /// <summary>
    /// Provides methods for drawing controls.
    /// </summary>
    public static class ControlDrawingHelpers
    {
        public const string BackgroundSolidBrushStyleKey = "BackgroundSolidBrushStyleKey_851A7F0D-0761-47D9-B786-5AE4E54DD90C";
        public const string ForegroundSolidBrushStyleKey = "ForegroundSolidBrushStyleKey_BC6F78A9-8BCD-4DAB-8EE8-9024F7266A9A";
        public const string BackgroundImageBrushStyleKey = "BackgroundImageBrushStyleKey_6F95C63D-2815-4E9F-8775-49FDA314E52F";
        public const string ForegroundImageBrushStyleKey = "ForegroundImageBrushStyleKey_A06AFF33-E8F1-4CA2-8C73-32AA57E32CCB";
        public const string ListBoxEntryStyleKey = "ListBoxEntryStyleKey_A3EA10C1-AEB0-4CDF-9E7A-1186970EDD96";
        public const string SelectedListBoxEntryStyleKey = "SelectedListBoxEntryStyleKey_6DAB9181-0994-4D4E-9BEB-0AA0367BD639";

        public const string ViewRect = "ViewRect_C563B726-D2C4-48BF-BA9C-AC2752DDD278";
        public const string RegainFocus = "RegainFocus_FCB552F0-EA33-44B6-8CB1-2908ECDC2094";
        public const string ControlName = "ControlName_04F79BAA-F043-4CC2-8978-1A8EC98C6F3B";
        public const string LastKeyboardControlID = "LastKeyboardControlID_0EBD0C3C-B167-473B-8815-BC6CE5BCC361";

        public static GUIStyle GetListBoxEntryStyle(this Control control)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            var style = control.GetStyle(ListBoxEntryStyleKey, false);
            if (style == null)
            {
                style = new GUIStyle(GUI.skin.label);
                control.Properties[ListBoxEntryStyleKey] = style;
            }

            return style;
        }

        public static GUIStyle GetSelectedListBoxEntryStyle(this Control control)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            var style = control.GetStyle(SelectedListBoxEntryStyleKey, false);
            if (style == null)
            {
                style = new GUIStyle(GUI.skin.label);

                var texture = new Texture2D(4, 4, TextureFormat.ARGB32, false);
                //TODO: setting texture pixeles here is slow don't do it
                var color = new Color32(51, 153, 255, 255);
                texture.SetPixels32(
                    new[]
                        {
                            color,color,color,color,
                            color,color,color,color,
                            color,color,color,color,
                            color,color,color,color
                        });

                texture.Apply();
                style.normal.background = texture;
                style.normal.textColor = UnityEngine.Color.white;
                control.Properties[SelectedListBoxEntryStyleKey] = style;
            }

            return style;
        }

        public static GUIStyle GetBackgroundBrushStyle(this Control control)
        {
            var brush = control.Background;
            if (brush != null)
            {
                if (brush is SolidColorBrush)
                {
                    return GetBrushStyle(control, brush, BackgroundSolidBrushStyleKey);
                }

                if (brush is ImageBrush)
                {
                    return GetBrushStyle(control, brush, BackgroundImageBrushStyleKey);
                }
            }

            return null;
        }

        public static GUIStyle GetForegroundBrushStyle(this Control control)
        {
            GUIStyle style = null;
            var brush = control.Foreground;
            if (brush != null)
            {
                if (brush is SolidColorBrush)
                {
                    return GetBrushStyle(control, brush, ForegroundSolidBrushStyleKey);
                }

                if (brush is ImageBrush)
                {
                    return GetBrushStyle(control, brush, ForegroundImageBrushStyleKey);
                }
            }

            return style;
        }

        private static GUIStyle GetBrushStyle(this Control control, Brush brush, string key)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            GUIStyle style = null;
            if (brush != null)
            {
                if (brush is SolidColorBrush)
                {
                    var solidBrush = brush as SolidColorBrush;
                    var color = new UnityEngine.Color(solidBrush.Color.R, solidBrush.Color.G, solidBrush.Color.B, solidBrush.Color.A);
                    color.a = solidBrush.Opacity;
                    style = control.GetStyle(key, true);

                    if (style.normal.background == null)
                    {
                        style.normal.background = new Texture2D(4, 4, TextureFormat.ARGB32, false);
                    }
                    //TODO: setting texture pixeles here is slow dont do it
                    style.normal.background.SetPixels(
                               new[] {
                                   color, color, color, color,
                                   color, color, color, color,
                                   color, color, color, color,
                                   color, color, color, color
                               });
                    style.normal.background.Apply();
                }
                else if (brush is ImageBrush)
                {
                    var imageBrush = brush as ImageBrush;
                    if (imageBrush.ImageSource is Texture2DSource)
                    {
                        style = control.GetStyle(key, true);
                        style.normal.background = ((Texture2DSource)imageBrush.ImageSource).Texture;
                    }
                }
            }

            return style;
        }

        public static void SetFontStyle(this GUIStyle style, Font font)
        {
            if (style == null)
            {
                throw new ArgumentNullException("style");
            }

            if (font == null || !(font is UnityFont))
            {
                throw new ArgumentNullException("font");
            }

            var fontReference = ((UnityFont)font).UnityFontReference;
            if (fontReference == null)
            {
                return;
            }

            style.font = fontReference;
            style.fontSize = fontReference.fontSize;
            if (!font.Bold && !font.Italic)
            {
                style.fontStyle = FontStyle.Normal;
            }
            else if (font.Bold && font.Italic)
            {
                style.fontStyle = FontStyle.BoldAndItalic;
            }
            else if (!font.Bold && font.Italic)
            {
                style.fontStyle = FontStyle.Italic;
            }
            else if (font.Bold && !font.Italic)
            {
                style.fontStyle = FontStyle.Bold;
            }

            style.fontSize = fontReference.fontSize;
        }

        public static GUIStyle GetStyle(this Control control, string key, bool? create)
        {
            return GetStyle(control, key, create, null);
        }

        public static GUIStyle GetStyle(this Control control, string key, bool? create, GUIStyle referenceStyle)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            var properties = control.Properties;

            var style = referenceStyle;
            object styleValue = null;
            if (properties != null && !properties.TryGetValue(key, out styleValue))
            {
                // style = (GUIStyle)styleValue;
                if (create.HasValue && create.Value)
                {
                    style = new GUIStyle(referenceStyle);
                    properties[key] = style;
                }
            }
            else
            {
                style = styleValue != null ? (GUIStyle)styleValue : style;
            }

            return style;
        }

        /// <summary>
        /// Returns an array of <see cref="GUILayoutOption"/> types based on the state of the control.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public static GUILayoutOption[] StandardDimentionOptions(Control control)
        {
            // check if null reference
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            var items = new GUILayoutOption[8];
            var count = 0;
            if (control.MinimumSize.Width > 0)
            {
                items[count++] = GUILayout.MinWidth(control.MinimumSize.Width);
            }

            if (control.MinimumSize.Height > 0)
            {
                items[count++] = GUILayout.MinHeight(control.MinimumSize.Height);
            }

            if (control.MaximumSize.Width != 0)
            {
                items[count++] = GUILayout.MaxWidth(control.MaximumSize.Width);
            }

            if (control.MaximumSize.Height != 0)
            {
                items[count++] = GUILayout.MaxHeight(control.MaximumSize.Height);
            }

            if (control.Width != 0)
            {
                items[count++] = GUILayout.Width(control.Width);
            }

            if (control.Height != 0)
            {
                //   Debug.Log("height");
                items[count++] = GUILayout.Height(control.Height);
            }

            if (control.Width == 0 && control.MinimumSize.Width == 0f && control.MaximumSize.Width == 0)
            {
                items[count++] = GUILayout.ExpandWidth(control.HorizontalAlignment == HorizontalAlignment.Stretch);
            }

            if (control.Height == 0 && control.MinimumSize.Height == 0f && control.MaximumSize.Height == 0)
            {
                items[count++] = GUILayout.ExpandHeight(control.VerticalAlignment == VerticalAlignment.Stretch);
            }

            Array.Resize(ref items, count);
            return items;
        }

        public static int SelectionList(int selected, object[] list, Action<int> callback)
        {
            return SelectionList(selected, list, "List Item", callback);
        }

        public static int SelectionList(int selected, object[] list, GUIStyle elementStyle, Action<int> callback)
        {
            for (var i = 0; i < list.Length; ++i)
            {
                var entry = list[i];
                if (entry == null)
                {
                    continue;
                }

                var guiContent = entry is GUIContent ? (GUIContent)entry : new GUIContent(entry.ToString());
                var elementRect = GUILayoutUtility.GetRect(guiContent, elementStyle);

                var current = Event.current;
                var hover = elementRect.Contains(current.mousePosition);
                if (hover && current.type == EventType.MouseDown && current.clickCount == 1)
                {
                    selected = i;
                    current.Use();
                }
                // check if changed from MouseUp to MouseDown
                else if (hover && callback != null && current.type == EventType.MouseDown && current.clickCount == 2)
                {
                    Debug.Log("Works !");
                    callback(i);
                    current.Use();
                }
                else if (current.type == EventType.repaint)
                {
                    elementStyle.Draw(elementRect, guiContent, hover, false, i == selected, false);
                }
            }

            return selected;
        }
        //#region List box

        /// <summary>
        /// Provides a delegate
        /// </summary>
        /// <param name="index"></param>
        // public delegate void DoubleClickCallback(int index);

        //public static int SelectionList(int selected, GUIContent[] list)
        //{
        //    return SelectionList(selected, list, "List Item", null);
        //}

        //public static int SelectionList(int selected, GUIContent[] list, GUIStyle elementStyle)
        //{
        //    return SelectionList(selected, list, elementStyle, null);
        //}

        //public static int SelectionList(int selected, GUIContent[] list, DoubleClickCallback callback)
        //{
        //    return SelectionList(selected, list, "List Item", callback);
        //}

        //public static int SelectionList(int selected, GUIContent[] list, GUIStyle elementStyle, DoubleClickCallback callback)
        //{
        //    for (int i = 0; i < list.Length; ++i)
        //    {
        //        Rect elementRect = GUILayoutUtility.GetRect(list[i], elementStyle);
        //        bool hover = elementRect.Contains(Event.current.mousePosition);
        //        if (hover && Event.current.type == EventType.MouseDown && Event.current.clickCount == 1) // added " && Event.current.clickCount == 1"
        //        {
        //            selected = i;
        //            Event.current.Use();
        //        }
        //        else if (hover && callback != null && Event.current.type == EventType.MouseDown && Event.current.clickCount == 2) //Changed from MouseUp to MouseDown
        //        {
        //            Debug.Log("Works !");
        //            callback(i);
        //            Event.current.Use();
        //        } 


        //        else if (Event.current.type == EventType.repaint)
        //        {
        //            elementStyle.Draw(elementRect, list[i], hover, false, i == selected, false);
        //        }
        //    }
        //    return selected;
        //}

        //public static int SelectionList(int selected, string[] list)
        //{
        //    return SelectionList(selected, list, "List Item", null);
        //}

        //public static int SelectionList(int selected, string[] list, GUIStyle elementStyle)
        //{
        //    return SelectionList(selected, list, elementStyle, null);
        //}

        //public static int SelectionList(int selected, string[] list, DoubleClickCallback callback)
        //{
        //    return SelectionList(selected, list, "List Item", callback);
        //}

        //public static int SelectionList(int selected, string[] list, GUIStyle elementStyle, DoubleClickCallback callback)
        //{
        //    for (int i = 0; i < list.Length; ++i)
        //    {
        //        Rect elementRect = GUILayoutUtility.GetRect(new GUIContent(list[i]), elementStyle);
        //        bool hover = elementRect.Contains(Event.current.mousePosition);
        //        if (hover && Event.current.type == EventType.MouseDown && Event.current.clickCount == 1) // added " && Event.current.clickCount == 1"
        //        {
        //            selected = i;
        //            Event.current.Use();
        //        }
        //        else if (hover && callback != null && Event.current.type == EventType.MouseDown && Event.current.clickCount == 2) //Changed from MouseUp to MouseDown
        //        {
        //            Debug.Log("Works !");
        //            callback(i);
        //            Event.current.Use();
        //        }
        //        else if (Event.current.type == EventType.repaint)
        //        {
        //            elementStyle.Draw(elementRect, list[i], hover, false, i == selected, false);
        //        }
        //    }
        //    return selected;
        //}
        //#endregion

    }
}
