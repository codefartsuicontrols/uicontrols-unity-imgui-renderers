﻿namespace Codefarts.UIControls.Renderers
{
    using System;
                                             
    using Codefarts.UIControls.Unity;

    using UnityEngine;

    /// <summary>
    /// Provides a rendering implementation for the <see cref="CheckBox"/> control.
    /// </summary>
    [ControlRenderer(typeof(CheckBox))]
    public class CheckBoxRenderer : BaseRenderer
    {
        /// <summary>
        /// The content for the check box.
        /// </summary>
        private GUIContent content;

        /// <summary>
        /// Initializes a new instance of the <see cref="ButtonRenderer"/> class.
        /// </summary>
        public CheckBoxRenderer()
        {
            this.content = new GUIContent();
        }
            
        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        public override void DrawControl(ControlRenderingArgs args)
        {
            var control = args.Control;
            var checkBox = control as CheckBox;
            if (checkBox == null)
            {
                throw new InvalidCastException("Expected CheckBox control.");
            }

            this.content.text = string.IsNullOrEmpty(checkBox.Text) ? string.Empty : checkBox.Text;
            this.content.image = null;
            this.content.tooltip = control.ToolTip;
            if (checkBox.Image is Texture2DSource)
            {
                this.content.image = ((Texture2DSource)checkBox.Image).Texture;
            }

            var rect = new Rect(control.Left, control.Top, control.Width, control.Height);// GUILayoutUtility.GetRect(1, Screen.width, 1, Screen.height, ControlDrawingHelpers.StandardDimentionOptions(control));
                                                                                          //var alwaysShowHorizontal = viewer.HorizontialScrollBarVisibility == ScrollBarVisibility.Visible;
                                                                                          //var alwaysShowVertical = viewer.VerticalScrollBarVisibility == ScrollBarVisibility.Visible;
                                                                                          //rect.x += control.Left;
                                                                                          //rect.y += control.Top;
            rect.xMax -= Math.Abs(control.Width) < float.Epsilon ? control.Right : 0;
            rect.yMax -= Math.Abs(control.Height) < float.Epsilon ? control.Bottom : 0;
            //rect.xMax -= alwaysShowHorizontal ? GUI.skin.verticalScrollbar.fixedWidth : 0;
            //rect.yMax -= alwaysShowVertical ? GUI.skin.horizontalScrollbar.fixedHeight : 0;

            if (rect.width <= 0 || rect.height <= 0)
            {
                return;
            }

            // draw the background brush
            var brush = checkBox.Background;
            if (brush != null)
            {
                BrushExtensions.Draw(brush, rect);
            }

            checkBox.IsChecked = GUI.Toggle(rect, checkBox.IsChecked, this.content, checkBox.Appearance == Appearance.Normal ? GUI.skin.toggle : GUI.skin.button);

            base.DrawControl(args);
        }
    }
}