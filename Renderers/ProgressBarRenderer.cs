﻿namespace Codefarts.UIControls.Renderers.Unity
{
    using Codefarts.UIControls.Renderers;

    using UnityEngine;

    /// <summary>
    /// Provides a renderer implementation for the <see cref="Slider"/> control.
    /// </summary>
    [ControlRenderer(typeof(ProgressBar))]
    public class ProgressBarRenderer : BaseRenderer
    {
        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        /// <exception cref="System.ArgumentNullException">control</exception>
        /// <exception cref="System.ArgumentException">Argument does not inherit from Slider.;control</exception>
        public override void DrawControl(ControlRenderingArgs args)
        {
            var progress = (ProgressBar)args.Control;

            var rect = new Rect(progress.Location + args.Offset, progress.Size);

            // draw background
            var brush = progress.Background;
            if (brush != null)
            {
                BrushExtensions.Draw(brush, rect);
            }

            KeyCode keyCode;
            bool isDown;
            bool isUp;
            string controlName;
            this.GetKeyInfoAndSetControlName(progress, out keyCode, out isDown, out isUp, out controlName, false);

            GUI.SetNextControlName(controlName);
            using (new GUI.GroupScope(rect))
            {
                // get style and sync it up
                var style = progress.GetStyle(Control.ControlStyle, true, GUI.skin.box);
                if (progress.Font != null)
                {
                    style.SetFontStyle(progress.Font);
                }

                var bounds = new Rect(Vector2.zero, rect.size);
                GUI.Box(bounds, string.Empty, GUI.skin.box);
                var textRect = bounds;
                bounds.width = ((progress.Value - progress.Minimum) / (progress.Maximum - progress.Minimum)) * rect.width;

                brush = progress.Foreground;
                if (brush != null)
                {
                    BrushExtensions.Draw(brush, bounds);
                }

                var fontStyle = new GUIStyle() { font = style.font, fontSize = style.fontSize, fontStyle = style.fontStyle };
                fontStyle.normal.textColor = Colors.White;
                fontStyle.alignment = TextAnchor.MiddleCenter;
                GUI.Label(textRect, new GUIContent(progress.Text) { tooltip = progress.ToolTip }, fontStyle);
            }

            // Handle key events
            this.HandleKeyEventsAfterControlDrawn(controlName, keyCode, isDown, progress, isUp);

            // handle mouse enter & leave events
          //  this.HandleMouseEvents(progress, args.Offset);

            base.DrawControl(args);
        }

        #region Overrides of BaseRenderer

        ///// <summary>
        ///// Updates the specified control.
        ///// </summary>
        ///// <param name="args">The rendering argument information.</param>
        ///// <remarks>
        ///// <p>This method is provided if a control has is animated and it's animation state can be updated independently of drawing.</p>
        ///// <p>Updates generally occur more frequently then draws.</p>
        ///// <p>This base renderer implementation will throw exceptions if the provided control is null or not of the expected type. </p>
        ///// </remarks>
        //public override void Update(ControlRenderingArgs args)
        //{
        //    var progress = (ProgressBar)args.Control;

        //    base.Update(args);

        //    if (args.Handled)
        //    {
        //        return;
        //    }

        //    BaseRenderer.QueueRefresh();
        //    // handle mouse enter & leave events
        ////    this.HandleMouseEvents(progress, args.Offset);
        //}

        #endregion
    }
}