﻿namespace Codefarts.UIControls.Renderers
{

    using UnityEngine;

    /// <summary>
    /// Provides a renderer implementation for the <see cref="ScrollViewer"/> control.
    /// </summary>
    [ControlRenderer(typeof(ScrollViewer))]
    public partial class ScrollViewerRenderer : BaseRenderer
    {
#if LEGACYUI
        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        /// <exception cref="System.ArgumentNullException">control</exception>
        public override void DrawControl(ControlRenderingArgs args)
        {
            var viewer = args.Control as ScrollViewer;

            var scrollPosition = new Vector2(viewer.HorizontialOffset, viewer.VerticalOffset);

            //var rect = new Rect(viewer.Left + args.Offset.X, viewer.Top + args.Offset.Y, viewer.Width, viewer.Height);
            var rect = new Rect(viewer.Left, viewer.Top, viewer.Width, viewer.Height);
            var alwaysShowHorizontal = viewer.HorizontialScrollBarVisibility == ScrollBarVisibility.Visible;
            var alwaysShowVertical = viewer.VerticalScrollBarVisibility == ScrollBarVisibility.Visible;

            var viewRect = new Rect(0, 0, rect.width, rect.height);
            object objectRef;
            if (viewer.Properties.TryGetValue(ControlDrawingHelpers.ViewRect, out objectRef))
            {
                viewRect = (Rect)objectRef;
            }

            // draw the background brush
            var brush = viewer.Background;
            if (brush != null)
            {
                var innerRect = rect;
                var width = alwaysShowVertical || viewRect.height > rect.height ? GUI.skin.verticalScrollbar.fixedWidth : 0;
                var height = alwaysShowHorizontal || viewRect.width > rect.width ? GUI.skin.horizontalScrollbar.fixedHeight : 0;
                innerRect.size -= new Vector2(width, height);
                BrushExtensions.Draw(brush, innerRect);
            }

            var isLayouting = Event.current.type == EventType.Layout;
            if (!alwaysShowHorizontal && !alwaysShowVertical)
            {
                using (new GUI.GroupScope(rect))
                {
                    // GUI.BeginGroup(new Rect(viewRect.x, viewRect.y, viewRect.width + viewRect.x, viewRect.height + viewRect.y));

                    this.DrawItems(args, isLayouting, rect);

                    // GUI.EndGroup();
                }
            }
            else
            {
                using (var scroll = new GUI.ScrollViewScope(rect, scrollPosition, viewRect, alwaysShowHorizontal, alwaysShowVertical))
                {
                    viewer.HorizontialOffset = scroll.scrollPosition.x;
                    viewer.VerticalOffset = scroll.scrollPosition.y;

                    GUI.BeginGroup(new Rect(viewRect.x, viewRect.y, viewRect.width + viewRect.x, viewRect.height + viewRect.y));

                    this.DrawItems(args, isLayouting, rect);

                    GUI.EndGroup();
                }
            }

            base.DrawControl(args);
        }

        protected virtual void DrawItems(ControlRenderingArgs args, bool isLayouting, Rect rect)
        {
            Control[] controls;
            if (args.Control.Controls == null)
            {
                return;
            }

            lock (args.Control.Controls)
            {
                controls = new Control[args.Control.Controls.Count];
                args.Control.Controls.CopyTo(controls, 0);
            }

            if (controls == null)
            {
                return;
            }

            if (isLayouting)
            {
                var viewRect = new Rect()
                {
                    xMin = float.MaxValue,
                    yMin = float.MaxValue,
                    xMax = float.MinValue,
                    yMax = float.MinValue
                };

                for (var i = 0; i < controls.Length; i++)   // expected draw order but click events are backward
                                                            //    for (var i = controls.Length - 1; i >= 0; i--)
                {
                    var child = controls[i];
                    if (!child.IsVisible)
                    {
                        continue;
                    }

                    viewRect.xMin = child.Left < viewRect.xMin ? child.Left : viewRect.xMin;
                    viewRect.yMin = child.Top < viewRect.yMin ? child.Top : viewRect.yMin;

                    var right = child.Width > 0 ? child.Left + child.Width : rect.xMax - child.Right;
                    var bottom = child.Height > 0 ? child.Top + child.Height : rect.yMax - child.Bottom;
                    viewRect.xMax = right > viewRect.xMax ? right : viewRect.xMax;
                    viewRect.yMax = bottom > viewRect.yMax ? bottom : viewRect.yMax;

                    // GUI.depth--;
                    args.Manager.DrawControl(child, args.ElapsedGameTime, args.TotalGameTime);
                }

                args.Control.Properties[ControlDrawingHelpers.ViewRect] = viewRect;
            }
            else
            {
                for (var i = 0; i < controls.Length; i++)
                //   for (var i = controls.Length - 1; i >= 0; i--)
                {
                    var child = controls[i];
                    if (child.IsVisible)
                    {
                        //  GUI.depth--;
                        args.Manager.DrawControl(child, args.ElapsedGameTime, args.TotalGameTime);
                    }
                }
            }
        }
#endif
    }
}