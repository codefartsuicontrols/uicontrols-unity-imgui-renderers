﻿namespace Codefarts.UIControls.Renderers
{
#if UNITY_5
    using System;

    using UnityEngine;

    /// <summary>
    /// Provides a renderer implementation for the <see cref="TextBox"/> control.
    /// </summary>
    [ControlRenderer(typeof(TextBox))]
    public class TextBoxRenderer : BaseRenderer
    {
        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        /// <exception cref="System.ArgumentNullException">control</exception>
        public override void DrawControl(ControlRenderingArgs args)
        {
            var textBox = (TextBox)args.Control;

            // unity gui does not like null strings
            var text = textBox.Text == null ? string.Empty : textBox.Text;

            var maxLength = textBox.MaxLength == 0 ? int.MaxValue : textBox.MaxLength;

            var rect = new Rect(textBox.Location + args.Offset, textBox.Size);
            var hsbVisibility = textBox.HorizontalScrollBarVisibility;
            var vsbVisibility = textBox.VerticalScrollBarVisibility;
            var alwaysShowHorizontal = hsbVisibility == ScrollBarVisibility.Visible;
            var alwaysShowVertical = vsbVisibility == ScrollBarVisibility.Visible;

            KeyCode keyCode;
            bool isDown;
            bool isUp;
            string controlName;
            this.GetKeyInfoAndSetControlName(textBox, out keyCode, out isDown, out isUp, out controlName, false);

            // get style and sync it up
            var style = textBox.GetStyle(Control.ControlStyle, true, textBox.AcceptsReturn ? GUI.skin.textArea : GUI.skin.textField);
            if (textBox.Font != null)
            {
                style.SetFontStyle(textBox.Font);
            }

            var current = Event.current;
            var lastKBFocus = -1;
            object objectRef;
            textBox.Properties.TryGetValue(ControlDrawingHelpers.LastKeyboardControlID, out objectRef);
            lastKBFocus = objectRef == null ? lastKBFocus : (int)objectRef;

            var selectionStart = textBox.SelectionStart;
            var selectionLength = textBox.SelectionLength;

            if (GUI.GetNameOfFocusedControl() == controlName && lastKBFocus == GUIUtility.keyboardControl)
            {
                var te = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);

                // sync textbox & texteditor selections
                this.SyncTextBoxSelection(TextBox.TextBoxSelectionStartChanged, te, textBox);
                this.SyncTextBoxSelection(TextBox.TextBoxSelectionLengthChanged, te, textBox);

                // process tab key if nessary
                text = this.HandleTabKeyPress(ref selectionStart, te, current, textBox, text);

                // handle cut / copy & paste operations
                text = this.HandleCutCopyPaste(ref selectionStart, current, te, text);

                selectionLength = Math.Abs(te.cursorIndex - te.selectIndex);
            }

            // draw the text area/field control
            textBox.Text = this.DrawActualTextControl(textBox, text, rect, alwaysShowHorizontal, alwaysShowVertical, style, maxLength, controlName);

            // check if we need to record last keyboard control id
            if (GUI.GetNameOfFocusedControl() == controlName && lastKBFocus != GUIUtility.keyboardControl)
            {
                textBox.Properties[ControlDrawingHelpers.LastKeyboardControlID] = GUIUtility.keyboardControl;
            }

            // check is text selection changed and sync if nessary
            if (textBox.SelectionStart != selectionStart)
            {
                textBox.SelectionStart = selectionStart;
            }
            if (textBox.SelectionLength != selectionLength)
            {
                textBox.SelectionLength = selectionLength;
            }

            // Handle key events
            this.HandleKeyEventsAfterControlDrawn(controlName, keyCode, isDown, textBox, isUp);

            // handle mouse enter & leave events
            //  this.HandleMouseEvents(textBox, args.Offset);

            base.DrawControl(args);
        }

        private string HandleCutCopyPaste(ref int selectionStart, Event current, TextEditor te, string text)
        {
            if (current == null)
            {
                throw new ArgumentNullException("current");
            }

            if (te == null)
            {
                throw new ArgumentNullException("te");
            }

            if (current.type == EventType.Used)
            {
                return text;
            }

            if (current.type == EventType.ValidateCommand)
            {
                switch (current.commandName)
                {
                    case "Cut":
                        if (te.hasSelection)
                        {
                            te.Cut();
                            text = te.text;
                            BaseRenderer.QueueRefresh();
                        }
                        break;

                    case "Copy":
                        if (te.hasSelection)
                        {
                            te.Copy();
                        }
                        break;

                    case "Paste":
                        te.Paste();
                        selectionStart = Math.Min(te.cursorIndex, te.selectIndex);
                        text = te.text;
                        BaseRenderer.QueueRefresh();
                        break;

                    case "SelectAll":
                        te.SelectAll();
                        selectionStart = Math.Min(te.cursorIndex, te.selectIndex);
                        BaseRenderer.QueueRefresh();
                        break;
                }
            }

            return text;
        }

        private string HandleTabKeyPress(ref int selectionStart, TextEditor te, Event current, TextBox textBox, string text)
        {
            if (current.type == EventType.Used)
            {
                return text;
            }

            selectionStart = Math.Min(te.cursorIndex, te.selectIndex);
            if (current.type == EventType.KeyDown || current.type == EventType.KeyUp)
            {
                if (current.isKey && (current.keyCode == KeyCode.Tab || current.character == '\t'))
                {
                    // consume the tab key event before drawing the control
                    if (current.type == EventType.KeyUp && textBox.AcceptsTab)
                    {
                        if (!current.shift)
                        {
                            for (var i = 0; i < 4; i++)
                            {
                                te.Insert(' ');
                            }
                        }
                        else
                        {
                            var min = selectionStart;
                            var index = min;
                            var temp = te.text;
                            for (var i = 1; i < 5; i++)
                            {
                                if ((min - i) < 0 || temp[min - i] != ' ')
                                {
                                    break;
                                }

                                index = min - i;
                            }

                            if (index < min)
                            {
                                te.selectIndex = index;
                                te.cursorIndex = min;
                                te.ReplaceSelection(string.Empty);
                            }
                        }

                        selectionStart = Math.Min(te.cursorIndex, te.selectIndex);

                        text = te.text;
                    }

                    current.Use();
                }
            }

            return text;
        }

        private void SyncTextBoxSelection(string name, TextEditor editor, TextBox tb)
        {
            var props = tb.Properties;
            if (props != null)
            {
                bool changed;
                object objectRef;
                if (props.TryGetValue(name, out objectRef))
                {
                    changed = (bool)objectRef;
                    if (changed)
                    {
                        props[name] = false;
                        this.SetTextEditorSelection(editor, tb.SelectionStart, tb.SelectionLength);
                    }
                }
            }
        }

        private void SetTextEditorSelection(TextEditor editor, int start, int length)
        {
            if (editor.cursorIndex < editor.selectIndex)
            {
                editor.cursorIndex = start;
                editor.selectIndex = start + length;
            }
            else
            {
                editor.selectIndex = start;
                editor.cursorIndex = start + length;
            }
        }

        protected virtual string DrawActualTextControl(TextBox textBox, string text, Rect rect, bool alwaysShowHorizontal, bool alwaysShowVertical,
            GUIStyle style, int maxLength, string controlName)
        {
            var scrollPosition = new Vector2(-textBox.HorizontalOffset, -textBox.VerticalOffset);
            if (textBox.AcceptsReturn)
            {
                var textSize = GUI.skin.textArea.CalcSize(new GUIContent(text));
                var viewRect = new Rect(Vector2.zero, textSize);
                viewRect.width = Math.Max(textSize.x, rect.width);
                viewRect.height = Math.Max(textSize.y, rect.height);

                var drawHorizScroll = viewRect.width > rect.width || alwaysShowHorizontal;
                var drawVertScroll = viewRect.height > rect.height || alwaysShowVertical;

                var horizRect = new Rect(
                    0,
                    rect.height - GUI.skin.horizontalScrollbar.fixedHeight,
                    rect.width - (drawVertScroll ? GUI.skin.verticalScrollbar.fixedWidth : 0),
                    GUI.skin.horizontalScrollbar.fixedHeight);
                var vertRect = new Rect(
                    rect.width - GUI.skin.verticalScrollbar.fixedWidth,
                    0,
                    GUI.skin.verticalScrollbar.fixedWidth,
                    rect.height - (drawHorizScroll ? GUI.skin.horizontalScrollbar.fixedHeight : 0));
                horizRect.position += rect.position;
                vertRect.position += rect.position;

                scrollPosition.x = drawHorizScroll ? scrollPosition.x : 0;
                scrollPosition.y = drawVertScroll ? scrollPosition.y : 0;

                viewRect.position = scrollPosition;
                var grpRect = new Rect(
                    rect.x,
                    rect.y,
                    rect.width - (drawVertScroll ? GUI.skin.verticalScrollbar.fixedWidth : 0),
                    rect.height - (drawHorizScroll ? GUI.skin.horizontalScrollbar.fixedHeight : 0));

                using (new GUI.GroupScope(grpRect))
                {
                    // draw background
                    var brush = textBox.Background;
                    if (brush != null)
                    {
                        BrushExtensions.Draw(brush, new Rect(Vector2.zero, grpRect.size));
                    }

                    GUI.SetNextControlName(controlName);
                    // as of unity v5.3 there is a bug that prevent me from specifying a maxlength
                    //BUG: see details here -> https://fogbugz.unity3d.com/default.asp?768436_vikdrmh7ernh03ls
                    text = GUI.TextArea(viewRect, text, style);
                }

                if (drawHorizScroll)
                {
                    textBox.HorizontalOffset = GUI.HorizontalScrollbar(
                        horizRect,
                        textBox.HorizontalOffset,
                        Math.Min(viewRect.width, rect.width),
                        0,
                        viewRect.width + (drawVertScroll ? GUI.skin.verticalScrollbar.fixedWidth : 0));
                }

                if (drawVertScroll)
                {
                    textBox.VerticalOffset = GUI.VerticalScrollbar(
                        vertRect,
                        textBox.VerticalOffset,
                        Math.Min(viewRect.height, rect.height),
                        0,
                        viewRect.height + (drawHorizScroll ? GUI.skin.horizontalScrollbar.fixedHeight : 0));
                }
            }
            else
            {
                // draw background
                var brush = textBox.Background;
                if (brush != null)
                {
                    BrushExtensions.Draw(brush, rect);
                }
                GUI.SetNextControlName(controlName);
                text = GUI.TextField(rect, text, maxLength, style);
            }

            return text;
        }
    }
#endif      
}