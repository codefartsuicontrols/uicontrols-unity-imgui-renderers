﻿namespace Codefarts.UIControls.Renderers
{
    using System;
    using UnityEngine;

    /// <summary>
    /// Provides a renderer implementation for the <see cref="Window"/> control.
    /// </summary>
    [ControlRenderer(typeof(Window))]
    public class WindowRenderer : ScrollViewerRenderer
    {
        public const string WindowID = "WindowID_CC83FC1C-B837-47D2-AA53-2B6D412A9E4D";

        /// <summary>
        /// The next window id identifier.
        /// </summary>
        private int nextWindowID;

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        public override void DrawControl(ControlRenderingArgs args)
        {
            var window = args.Control as Window;
            if (window == null)
            {
                throw new InvalidCastException();
            }

            var windowRect = new Rect(window.Left, window.Top, window.Width, window.Height);

            int id;
            object objectRef;
            if (!window.Properties.TryGetValue(WindowRenderer.WindowID, out objectRef))
            {
                id = this.nextWindowID++;
                window.Properties[WindowRenderer.WindowID] = id;
            }
            else
            {
                id = (int)objectRef;
            }
                                    
            windowRect = GUI.Window(
                id,
                windowRect,
                x =>
                {
                    var controls = window.Controls;
                    if (controls != null)
                    {
                        foreach (var child in controls)
                        {
                            args.Manager.DrawControl(child, args.ElapsedGameTime, args.TotalGameTime);
                        }
                    }

                    // if (window.IsDragable)
                    {
                        GUI.DragWindow();
                    }
                },
                window.Title, GUI.skin.window);

            window.SetBounds(windowRect.x, windowRect.y, windowRect.width, windowRect.height);

            base.DrawControl(args);
        }
    }
}