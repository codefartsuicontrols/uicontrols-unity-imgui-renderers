﻿namespace Codefarts.UIControls.Renderers
{
    using System;

    using Codefarts.UIControls.Controls;     
    using Codefarts.UIControls.Unity;

    using UnityEngine;

    /// <summary>
    /// Provides a renderer implementation for the <see cref="Image"/> control.
    /// </summary>
    [ControlRenderer(typeof(Image))]
    public class ImageRenderer : BaseRenderer
    {        
        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        public override void DrawControl(ControlRenderingArgs args)
        {
            var control = args.Control;
            var image = control as Image;
            if (image == null)
            {
                throw new InvalidCastException();
            }

            if (!(image.Source is Texture2DSource))
            {
                return;
            }

            var source = image.Source as Texture2DSource;

            var scale = ScaleMode.StretchToFill;
            switch (image.Stretch)
            {
                case Stretch.Fill:
                    scale = ScaleMode.StretchToFill;
                    break;

                case Stretch.None:
                    scale = ScaleMode.ScaleAndCrop;
                    break;

                case Stretch.Uniform:
                    scale = ScaleMode.ScaleToFit;
                    break;

                case Stretch.UniformToFill:
                    scale = ScaleMode.StretchToFill;
                    break;
            }

            // figure out dimensions
            var width = Math.Max(control.MinimumSize.Width, control.Width);
            var height = Math.Max(control.MinimumSize.Height, control.Height);

            switch (scale)
            {
                case ScaleMode.StretchToFill:
                    break;

                case ScaleMode.ScaleAndCrop:
                    break;

                case ScaleMode.ScaleToFit:
                    width = Math.Min(image.Source.Width, width);
                    height = Math.Min(image.Source.Height, height);
                    break;
            }
                                                              
            var rect = new Rect(control.Left, control.Top, width, height);
            GUI.DrawTexture(rect, source.Texture, scale, true);
        }
    }
}