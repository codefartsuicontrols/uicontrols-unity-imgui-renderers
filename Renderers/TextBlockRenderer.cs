﻿namespace Codefarts.UIControls.Renderers
{
#if UNITY_5
    using System;
    using UnityEngine;
    using TextAlignment = Codefarts.UIControls.TextAlignment;

    /// <summary>
    /// Provides a renderer implementation for the <see cref="TextBlock"/> control.
    /// </summary>
    [ControlRenderer(typeof(TextBlock))]
    public class TextBlockRenderer : BaseRenderer
    {
        //  private GUIStyle style;

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        public override void DrawControl(ControlRenderingArgs args)
        {
            var control = args.Control;
            var textBlock = control as TextBlock;
            if (textBlock == null)
            {
                throw new InvalidCastException("Expected TextBlock control.");
            }

            var rect = new Rect(control.Left, control.Top, control.Width, control.Height);
            rect.xMax -= control.Width == 0f ? control.Right + control.Left : 0;
            rect.yMax -= control.Height == 0f ? control.Bottom + control.Top : 0;

            // draw background
            var brush = textBlock.Background;
            if (brush != null)
            {
                BrushExtensions.Draw(brush, rect);
            }

            var style = textBlock.GetStyle(Control.ControlStyle, true, GUI.skin.label);

            switch (textBlock.TextAlignment)
            {
                case TextAlignment.Left:
                    style.alignment = TextAnchor.MiddleLeft;
                    break;

                case TextAlignment.Right:
                    style.alignment = TextAnchor.MiddleRight;
                    break;

                case TextAlignment.Center:
                    style.alignment = TextAnchor.MiddleCenter;
                    break;

                case TextAlignment.Justify:
                    // not uspported
                    break;
            }

            style.wordWrap = textBlock.TextWrapping == TextWrapping.Wrap;
            style.clipping = textBlock.TextTrimming == TextTrimming.None ? TextClipping.Overflow : TextClipping.Clip;
            //style.Draw(rect, new GUIContent(textBlock.Text), false, control.IsEnabled, false, false);
            GUI.Label(rect, textBlock.Text, style);

            // this.DrawDebugFrames(rect, control);
        }
    }
#endif
}