﻿namespace Codefarts.UIControls.Renderers.Unity
{
    using System;
                                          
    using UnityEngine;

    /// <summary>
    /// Provides a renderer implementation for the <see cref="Slider"/> control.
    /// </summary>
    public class SliderRenderer : BaseRenderer
    {                      
        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        /// <exception cref="System.ArgumentNullException">control</exception>
        /// <exception cref="System.ArgumentException">Argument does not inherit from Slider.;control</exception>
        public override void DrawControl(ControlRenderingArgs args)
        {
            var control = args.Control;
            var slider = control as Slider;
            float value;
            switch (slider.Orientation)
            {
                case Orientation.Horizontal:
                    value = GUILayout.HorizontalSlider(slider.Value, slider.Minimum, slider.Maximum, ControlDrawingHelpers.StandardDimentionOptions(slider));
                    if (slider.IsEnabled)
                    {
                        slider.Value = value;
                    }

                    break;

                case Orientation.Vertical:
                    value = GUILayout.VerticalSlider(slider.Value, slider.Minimum, slider.Maximum, ControlDrawingHelpers.StandardDimentionOptions(slider));
                    if (slider.IsEnabled)
                    {
                        slider.Value = value;
                    }

                    break;
            }
        }
    }
}