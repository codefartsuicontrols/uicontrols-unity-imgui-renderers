﻿namespace Codefarts.UIControls.Renderers
{
    using System;

    using Codefarts.UIControls.Interfaces;
#if UNITY_5
    using UnityEngine;
#endif

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="StackPanel"/> control.
    /// </summary>
    [ControlRenderer(typeof(StackPanel))]
    public partial class StackPanelRenderer : ScrollViewerRenderer
    {
#if LEGACYUI

        ///// <summary>
        ///// Implemented by inheritors to draw the actual control.
        ///// </summary>
        ///// <param name="args"></param>
        //public override void DrawControl(ControlRenderingArgs args)
        //{
        //    var control = args.Control;
        //    var panel = control as StackPanel;

        //    var style = control.GetBackgroundBrushStyle();
        //    var rect = new Rect(control.Left, control.Top, control.Width, control.Height);

        //    // draw background
        //    var brush = panel.Background;
        //    if (brush != null)
        //    {
        //        BrushExtensions.Draw(brush, rect);
        //    }

        //    if (rect.width <= 0 || rect.height <= 0)
        //    {
        //        return;
        //    }

        //    var useBeginArea = panel.Left != 0 || panel.Top != 0;
        //    if (useBeginArea)
        //    {
        //        if (style != null)
        //        {
        //            GUILayout.BeginArea(new Rect(panel.Left, panel.Top, panel.Width, panel.Height), style);
        //        }
        //        else
        //        {
        //            GUILayout.BeginArea(new Rect(panel.Left, panel.Top, panel.Width, panel.Height));
        //        }
        //    }

        //    if (panel.Orientation == Orientation.Horizontal)
        //    {
        //        if (style != null)
        //        {
        //            //  Debug.Log("clipping " + style.clipping.ToString());
        //            // style.clipping = TextClipping.Clip;

        //            GUILayout.BeginHorizontal(style, ControlDrawingHelpers.StandardDimentionOptions(control));
        //        }
        //        else
        //        {
        //            GUILayout.BeginHorizontal(ControlDrawingHelpers.StandardDimentionOptions(control));
        //        }
        //    }
        //    else
        //    {
        //        if (style != null)
        //        {
        //            GUILayout.BeginVertical(style, ControlDrawingHelpers.StandardDimentionOptions(control));
        //        }
        //        else
        //        {
        //            GUILayout.BeginVertical(ControlDrawingHelpers.StandardDimentionOptions(control));
        //        }
        //    }

        //    try
        //    {
        //        Control[] children;
        //        lock (panel.Controls)
        //        {
        //            children = new Control[panel.Controls.Count];
        //            panel.Controls.CopyTo(children, 0);
        //        }

        //        if (children != null)
        //        {
        //            // TODO: Horrible code is this contains a large set of items extreme slow down
        //            Array.Sort(
        //                children,
        //                (x, y) =>
        //                {
        //                    var indexX = Array.IndexOf(children, x);
        //                    var indexY = Array.IndexOf(children, y);

        //                    if (x is IControlGroup)
        //                    {
        //                        indexX = (x as IControlGroup).Group;
        //                    }

        //                    if (indexX < indexY)
        //                    {
        //                        return -1;
        //                    }

        //                    if (indexX > indexY)
        //                    {
        //                        return 1;
        //                    }

        //                    return 0;
        //                });

        //            var doClip = control.ClipToBounds;
        //            string groupId = null;
        //            if (doClip)
        //            {
        //                groupId = control.Properties.ContainsKey("BeginGroup") ? (string)control.Properties["BeginGroup"] : Guid.NewGuid().ToString();
        //                control.Properties["BeginGroup"] = groupId;
        //                // Debug.Log(groupId);  
        //                GUILayoutUtility.BeginGroup(groupId);
        //            }

        //            foreach (var child in children)
        //            {
        //                args.Manager.DrawControl(child, args.ElapsedGameTime, args.TotalGameTime);
        //            }

        //            if (doClip)
        //            {
        //                groupId = (string)control.Properties["BeginGroup"];
        //                GUILayoutUtility.EndGroup(groupId);
        //            }
        //        }
        //    }
        //    finally
        //    {
        //        if (panel.Orientation == Orientation.Horizontal)
        //        {
        //            GUILayout.EndHorizontal();
        //        }
        //        else
        //        {
        //            GUILayout.EndVertical();
        //        }
        //    }

        //    if (useBeginArea)
        //    {
        //        GUILayout.EndArea();
        //    }

        //    // handle mouse enter & leave events                 
        //    this.HandleMouseEvents(control);
        //}

        protected override void DrawItems(ControlRenderingArgs args, bool isLayouting, Rect rect)
        {
            var panel = args.Control as StackPanel;
            float position = 0;

            var controls = panel.Controls;
            if (controls == null)
            {
                return;
            }

            if (isLayouting)
            {
                var viewRect = new Rect()
                {
                    xMin = float.MaxValue,
                    yMin = float.MaxValue,
                    xMax = float.MinValue,
                    yMax = float.MinValue
                };

                foreach (var child in controls)
                {
                    if (!child.IsVisible)
                    {
                        continue;
                    }

                    switch (panel.Orientation)
                    {
                        case Orientation.Horizontal:
                            child.SetBounds(position, 0, child.Width, panel.Height);
                            position += child.Width;
                            break;

                        case Orientation.Vertical:
                            child.SetBounds(0, position, panel.Width, child.Height);
                            position += child.Height;
                            break;
                    }

                    viewRect.xMin = child.Left < viewRect.xMin ? child.Left : viewRect.xMin;
                    viewRect.yMin = child.Top < viewRect.yMin ? child.Top : viewRect.yMin;

                    var right = child.Width > 0 ? child.Left + child.Width : rect.xMax - child.Right;
                    var bottom = child.Height > 0 ? child.Top + child.Height : rect.yMax - child.Bottom;
                    viewRect.xMax = right > viewRect.xMax ? right : viewRect.xMax;
                    viewRect.yMax = bottom > viewRect.yMax ? bottom : viewRect.yMax;

                    args.Manager.DrawControl(child, args.ElapsedGameTime, args.TotalGameTime);
                }

                args.Control.Properties[ControlDrawingHelpers.ViewRect] = viewRect;
            }
            else
            {
                foreach (var child in controls)
                {
                    if (!child.IsVisible)
                    {
                        continue;
                    }

                    switch (panel.Orientation)
                    {
                        case Orientation.Horizontal:
                            child.SetBounds(position, 0, child.Width, panel.Height);
                            position += child.Width;
                            break;

                        case Orientation.Vertical:
                            child.SetBounds(0, position, panel.Width, child.Height);
                            position += child.Height;
                            break;
                    }

                    args.Manager.DrawControl(child, args.ElapsedGameTime, args.TotalGameTime);
                }
            }
        }
#endif
    }
}