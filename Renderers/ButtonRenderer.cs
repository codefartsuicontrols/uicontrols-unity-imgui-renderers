﻿namespace Codefarts.UIControls.Renderers
{
    using Codefarts.UIControls.Unity;

    using UnityEngine;

#if !LEGACYUI
    using UnityEngine.UI;
#endif                                           

    [ControlRenderer(typeof(Button))]
    public partial class ButtonRenderer : BaseRenderer
    {
#if LEGACYUI
        private GUIContent content = new GUIContent();

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        public override void DrawControl(ControlRenderingArgs args)
        {
            var button = (Button)args.Control;

            // setup button text
            this.content.text = button.Text;
            this.content.image = null;
            this.content.tooltip = button.ToolTip;
            if (button.Image is Texture2DSource)
            {
                this.content.image = ((Texture2DSource)button.Image).Texture;
            }

            var rect = new Rect(button.Location + args.Offset, button.Size);
            if (rect.width <= 0 || rect.height <= 0)
            {
                return;
            }

            // draw background
            var brush = button.Background;
            if (brush != null)
            {
                if (brush is SkinningBrush)
                {
                    var skinBrush = brush as SkinningBrush;
                    BrushExtensions.Draw(skinBrush, rect, button.IsFocused, !button.IsEnabled, false, button.IsMouseOver);
                }
                else
                {
                    BrushExtensions.Draw(brush, rect);
                }
            }

            var style = button.GetStyle(Control.ControlStyle, true, GUI.skin.button);

            // var font = button.Font as UnityFont;
            // if (font != null)
            // {
            // // draw button text
            // GUI.Label(rect, content);
            // }
            if (GUI.Button(rect, this.content, style))
            {
                button.OnClick();
            }

            // GUI.skin.button.Draw(rect, content, button.IsMouseOver, false, !button.IsEnabled, button.IsFocused);
            base.DrawControl(args);
        }

#endif
    }
}
