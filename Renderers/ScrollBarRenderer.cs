﻿namespace Codefarts.UIControls.Renderers.Unity
{                 
    using Codefarts.UIControls.Renderers;

    using UnityEngine;

    /// <summary>
    /// Provides a renderer implementation for the <see cref="ScrollBar"/> control.
    /// </summary>
    public class ScrollBarRenderer : BaseRenderer
    {                 
        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        /// <exception cref="System.ArgumentNullException">control</exception>
        /// <exception cref="System.ArgumentException">Argument does not inherit from ScrollBar.;control</exception> 
        public override void DrawControl(ControlRenderingArgs args)
        {
            var scrollBar = (ScrollBar)args.Control;
            switch (scrollBar.Orientation)
            {
                case Orientation.Horizontal:
                    scrollBar.Value = GUILayout.HorizontalScrollbar(scrollBar.Value, 1, scrollBar.Minimum, scrollBar.Maximum, ControlDrawingHelpers.StandardDimentionOptions(scrollBar));
                    break;

                case Orientation.Vertical:
                    scrollBar.Value = GUILayout.VerticalScrollbar(scrollBar.Value, 1, scrollBar.Minimum, scrollBar.Maximum, ControlDrawingHelpers.StandardDimentionOptions(scrollBar));
                    break;
            }
        }
    }                                                                                                            
}