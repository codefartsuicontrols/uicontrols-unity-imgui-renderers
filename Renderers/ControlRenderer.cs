namespace Codefarts.UIControls.Renderers
{
    using UnityEngine;

    [ControlRenderer(typeof(Control))]
    public class ControlRenderer : BaseRenderer
    {
        #region Overrides of BaseRenderer

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        public override void DrawControl(ControlRenderingArgs args)
        {
            // draw background
            var control = args.Control;
            if (control == null)
            {
                return;
            }

            var rect = new Rect(control.Location + args.Offset, control.Size);
            if (rect.width <= 0 || rect.height <= 0)
            {
                return;
            }

            var brush = control.Background;
            if (brush != null)
            {
                if (brush is SkinningBrush)
                {
                    var skinBrush = brush as SkinningBrush;
                    BrushExtensions.Draw(skinBrush, rect, control.IsFocused, !control.IsEnabled, false, control.IsMouseOver);
                }
                else
                {
                    BrushExtensions.Draw(brush, rect);
                }
            }

            Control[] controls;
            if (control.Controls == null)                                                     
            {
                return;
            }

            lock (control.Controls)
            {
                controls = new Control[control.Controls.Count];
                control.Controls.CopyTo(controls, 0);
            }

            if (controls == null)
            {
                return;
            }

            using (new GUI.GroupScope(rect))
            {
                for (var i = 0; i < controls.Length; i++)
                {
                    var child = controls[i];
                    if (child.IsVisible)
                    {
                        args.Manager.DrawControl(child, args.ElapsedGameTime, args.TotalGameTime);
                    }
                }
            }

            base.DrawControl(args);
        }

        #endregion
    }
}