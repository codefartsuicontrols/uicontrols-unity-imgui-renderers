﻿namespace Codefarts.UIControls.Renderers
{
    using UnityEngine;

    [ControlRenderer(typeof(Splitter))]
    public partial class SplitterRenderer : BaseRenderer
    {
#if LEGACYUI
        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        public override void DrawControl(ControlRenderingArgs args)
        {
            var button = (Splitter)args.Control;

            // setup button text
            //this.content.text = button.Text;
            //this.content.image = null;
            //this.content.tooltip = button.ToolTip;
            //if (button.Image is Texture2DSource)
            //{
            //    this.content.image = ((Texture2DSource)button.Image).Texture;
            //}

            var positionPoint = button.Location + args.Offset;
            var positionVector = new Vector2(positionPoint.X, positionPoint.Y);
            var rect = new Rect(positionVector, new Vector2(button.Width, button.Height));
            if (rect.width <= 0 || rect.height <= 0)
            {
                return;
            }

            // draw background
            var brush = button.Background;
            if (brush != null)
            {
                if (brush is SkinningBrush)
                {
                    var skinBrush = brush as SkinningBrush;
                    BrushExtensions.Draw(skinBrush, rect, button.IsFocused, !button.IsEnabled, false, button.IsMouseOver);
                }
                else
                {
                    BrushExtensions.Draw(brush, rect);
                }
            }

            base.DrawControl(args);
        }
#endif
    }
}