namespace Codefarts.UIControls.Renderers
{
    using System;
    using System.Linq;

    using UnityEngine;

    [ControlRenderer(typeof(Expander))]
    public partial class ExpanderRenderer : BaseRenderer
    {
        private Texture2D downTexture;
        private Texture2D upTexture;

        private GUIStyle buttonSkin;

        private void CreateTextures()
        {
            if (this.buttonSkin == null)
            {
                this.buttonSkin = new GUIStyle(GUI.skin.box);
                this.buttonSkin.imagePosition = ImagePosition.ImageLeft;
                this.buttonSkin.stretchWidth = true;
                this.buttonSkin.alignment = TextAnchor.MiddleLeft;
                this.buttonSkin.onActive = GUI.skin.button.onActive;
                this.buttonSkin.active = GUI.skin.button.active;
                this.buttonSkin.normal.textColor = GUI.skin.label.normal.textColor;
            }

            this.downTexture = new Texture2D(9, 9, TextureFormat.ARGB32, false);
            this.upTexture = new Texture2D(9, 9, TextureFormat.ARGB32, false);

            var t = Color.clear;
            var c = Color.gray;
            this.downTexture.SetPixels(new[]
                {
                   t,t,c,t,t,t,t,t,t,
                   t,t,c,c,t,t,t,t,t,
                   t,t,c,c,c,t,t,t,t,
                   t,t,c,c,c,c,t,t,t,
                   t,t,c,c,c,c,c,t,t,
                   t,t,c,c,c,c,t,t,t,
                   t,t,c,c,c,t,t,t,t,
                   t,t,c,c,t,t,t,t,t,
                   t,t,c,t,t,t,t,t,t,
            });
            this.downTexture.Apply();

            this.upTexture.SetPixels(new[]
               {
                   t,t,t,t,t,t,t,t,t,
                   t,t,t,t,t,t,t,t,t,
                   c,c,c,c,c,c,c,c,c,
                   t,c,c,c,c,c,c,c,t,
                   t,t,c,c,c,c,c,t,t,
                   t,t,t,c,c,c,t,t,t,
                   t,t,t,t,c,t,t,t,t,
                   t,t,t,t,t,t,t,t,t,
                   t,t,t,t,t,t,t,t,t,
            });
            this.upTexture.Apply();
        }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        public override void DrawControl(ControlRenderingArgs args)
        {
            var control = args.Control;
            var expander = control as Expander;
            if (expander == null)
            {
                throw new InvalidCastException("Expected Expander control.");
            }

            this.CreateTextures();

            var rect = new Rect(control.Left, control.Top, control.Width, control.Height);
            rect.position += args.Offset;
            var expanderTexture = expander.IsExpanded ? this.upTexture : this.downTexture;
            var content = new GUIContent(expander.Text, expanderTexture);
            expander.IsExpanded = GUI.Toggle(rect, expander.IsExpanded, content, this.buttonSkin);

            if (expander.IsExpanded && expander.Content != null)
            {
                // draw container control
                var childRect = new Rect(rect.x, rect.yMax, rect.width, expander.Content.Height);
                expander.Content.Bounds = childRect;
                args.Manager.DrawControl(expander.Content, args.ElapsedGameTime, args.TotalGameTime);
            }

            base.DrawControl(args);
        }
    }
}