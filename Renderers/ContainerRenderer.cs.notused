﻿namespace Codefarts.UIControls.Code.Renderers
{
    using System;

    using Codefarts.UIControls.Interfaces;

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="ItemsControl"/>.
    /// </summary>
    public partial class ContainerRenderer : BaseRenderer
    {
        /// <summary>
        /// Gets the type of the control that the renderer is designed to render.
        /// </summary>
        public override Type ControlType
        {
            get
            {
                return typeof(ItemsControl);
            }
        }

#if LEGACYUI
        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="manager">The manager containing all control renderers.</param>
        /// <param name="control">The control to be updated.</param>
        /// <param name="elapsedGameTime">The elapsed game time.</param>
        /// <param name="totalGameTime">The total game time.</param>
        public override void DrawControl(IControlRendererManager manager, Control control, float elapsedGameTime, float totalGameTime)
        {
            var containerControl = control as ItemsControl;
            switch (control.Visibility)
            {
                case Visibility.Visible:
                    GUI.enabled = GUI.enabled && control.IsEnabled;
                    var style = control.GetBackgroundBrushStyle();
                    if (style != null)
                    {
                        GUILayout.BeginVertical(style, ControlDrawingHelpers.StandardDimentionOptions(control));
                    }
                    else
                    {
                        GUILayout.BeginVertical(ControlDrawingHelpers.StandardDimentionOptions(control));
                    }

                    try
                    {
                        Control[] tools;
                        lock (containerControl.Children)
                        {
                            tools = new Control[containerControl.Children.Count];
                            containerControl.Children.CopyTo(tools, 0);
                        }

                        if (tools != null)
                        {
                            Array.Sort(
                                tools,
                                (x, y) =>
                                {
                                    var indexX = Array.IndexOf(tools, x);
                                    var indexY = Array.IndexOf(tools, y);

                                    if (x is IControlGroup)
                                    {
                                        indexX = (x as IControlGroup).Group;
                                    }

                                    if (indexX < indexY)
                                    {
                                        return -1;
                                    }

                                    if (indexX > indexY)
                                    {
                                        return 1;
                                    }

                                    return 0;
                                });

                            foreach (var item in tools)
                            {
                                manager.DrawControl(item, elapsedGameTime, totalGameTime);
                            }
                        }
                    }
                    finally
                    {
                        GUILayout.EndVertical();
                    }

                    break;

                case Visibility.Hidden:
                    break;

                case Visibility.Collapsed:
                    break;
            }
        } 
#endif
    }
}