﻿namespace Codefarts.UIControls.Renderers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
#if UNITY_5
    using UnityEngine;

    /// <summary>
    /// Provides a renderer implementation for the <see cref="ListBox"/> control.
    /// </summary>
    [ControlRenderer(typeof(ListBox))]
    public class ListBoxRenderer : ScrollViewerRenderer
    {
        /// <summary>
        /// Holds a cached <see cref="ListBoxItemInformationArgs"/> reference.
        /// </summary>
        ListBoxItemInformationArgs measureItemArgs = new ListBoxItemInformationArgs();

        private Stack<ControlRenderingArgs> args;

        /// <summary>
        /// Initializes a new instance of the <see cref="ListBoxRenderer"/> class.
        /// </summary>
        public ListBoxRenderer()
        {
            this.args = new Stack<ControlRenderingArgs>();
        }

        #region Overrides of ScrollViewerRenderer

        protected override void DrawItems(ControlRenderingArgs args, bool isLayouting, Rect rect)
        {
            object[] items;
            var listbox = args.Control as ListBox;
            if (listbox == null)
            {
                throw new InvalidCastException("Expected ListBox control.");
            }

            var dataSource = listbox.DataContext as IEnumerable;
            if (dataSource != null)
            {
                lock (dataSource)
                {
                    var list = new List<object>();
                    foreach (var item in dataSource)
                    {
                        list.Add(item);
                    }

                    items = list.ToArray();
                }
            }
            else
            {
                lock (listbox.Items)
                {
                    items = new object[listbox.Items.Count];
                    listbox.Items.CopyTo(items, 0);
                }
            }

            switch (listbox.DrawMode)
            {
                case DrawMode.Normal:
                    this.SelectionList(args, listbox, listbox.SelectedIndicies, items, isLayouting, rect, null);
                    break;

                case DrawMode.OwnerDraw:
                    this.DoOwnerDraw(items, listbox);
                    break;
            }
        }

        private void DoOwnerDraw(object[] items, ListBox listbox)
        {
            var sizes = new Vector2[items.Length];

            // measure all items and store there sizes in the sizes array
            for (var i = 0; i < items.Length; i++)
            {
                this.measureItemArgs.Item = items[i];
                this.measureItemArgs.Index = i;
                listbox.OnMeasureItem(this.measureItemArgs);
                sizes[i] = new Vector2(this.measureItemArgs.ItemWidth, this.measureItemArgs.ItemHeight);
            }

            // draw all the items
            for (var i = 0; i < items.Length; i++)
            {
                this.measureItemArgs.Item = items[i];
                this.measureItemArgs.Index = i;
                var size = sizes[i];
                this.measureItemArgs.ItemWidth = size.x;
                this.measureItemArgs.ItemHeight = size.y;
                listbox.OnDrawItem(this.measureItemArgs);
            }
        }

        #endregion

        private void SelectionList(ControlRenderingArgs args, ListBox listbox, ListBox.SelectedIndexCollection selectedIndexs, object[] list, bool isLayouting, Rect rect, Action<int> doubleClickCallback)
        {
            var selectedStyle = args.Control.GetSelectedListBoxEntryStyle();
            var normalStyle = args.Control.GetListBoxEntryStyle();

            if (isLayouting)
            {
                var viewRect = new Rect()
                {
                    xMin = float.MaxValue,
                    yMin = float.MaxValue,
                    xMax = float.MinValue,
                    yMax = float.MinValue
                };

                var prevElementRect = new Rect();
                var maxWidth = 0f;
                for (var i = 0; i < list.Length; ++i)
                {
                    var child = list[i];
                    if (child == null)
                    {
                        continue;
                    }

                    var callback = listbox.DisplayMemberCallback;
                    var text = callback != null ? callback(child) : child.ToString();
                    var guiContent = child is GUIContent ? (GUIContent)child : new GUIContent(text);
                    var elementStyle = selectedIndexs.Contains(i) ? selectedStyle : normalStyle;
                    var elementRect = new Rect(prevElementRect.position + new Vector2(0, prevElementRect.height), elementStyle.CalcSize(guiContent));
                    maxWidth = maxWidth < elementRect.width ? elementRect.width : maxWidth;

                    viewRect.xMin = elementRect.x < viewRect.xMin ? elementRect.x : viewRect.xMin;
                    viewRect.yMin = elementRect.y < viewRect.yMin ? elementRect.y : viewRect.yMin;

                    var right = elementRect.width > 0 ? elementRect.x + elementRect.width : rect.xMax - elementRect.xMax;
                    var bottom = elementRect.height > 0 ? elementRect.y + elementRect.height : rect.yMax - elementRect.yMax;
                    viewRect.xMax = right > viewRect.xMax ? right : viewRect.xMax;
                    viewRect.yMax = bottom > viewRect.yMax ? bottom : viewRect.yMax;

                    prevElementRect = elementRect;
                }

                // logic to ensure that the view rect expands to the full width of the listbox minus the width of the vertical
                // scroll bar if it is visible
                if (listbox.VerticalScrollBarVisibility == ScrollBarVisibility.Visible ||
                    (viewRect.height > rect.height && listbox.VerticalScrollBarVisibility == ScrollBarVisibility.Auto))
                {
                    var fixedWidth = GUI.skin.verticalScrollbar.fixedWidth;
                    if (viewRect.width < rect.width - fixedWidth)
                    {
                        viewRect.width = rect.width - fixedWidth;
                    }
                }
                else
                {
                    viewRect.width = maxWidth > rect.width ? maxWidth : rect.width;
                }

                args.Control.Properties[ControlDrawingHelpers.ViewRect] = viewRect;
            }
            else
            {
                var prevElementRect = new Rect();
                var viewRect = new Rect(0, 0, rect.width, rect.height);
                object objectRef;
                if (listbox.Properties.TryGetValue(ControlDrawingHelpers.ViewRect, out objectRef) && objectRef != null)
                {
                    viewRect = (Rect)objectRef;
                }

                for (var i = 0; i < list.Length; ++i)
                {
                    var child = list[i];
                    if (child == null)
                    {
                        continue;
                    }

                    var callback = listbox.DisplayMemberCallback;
                    var text = callback != null ? callback(child) : child.ToString();
                    var guiContent = child is GUIContent ? (GUIContent)child : new GUIContent(text);
                    var isSelected = selectedIndexs.Contains(i);
                    var elementStyle = isSelected ? selectedStyle : normalStyle;
                    var elementRect = new Rect(prevElementRect.position + new Vector2(viewRect.x, prevElementRect.height), elementStyle.CalcSize(guiContent));
                    elementRect.width = viewRect.width;

                    var current = Event.current;
                    var hover = elementRect.Contains(current.mousePosition);
                    if (hover && current.type == EventType.MouseDown && current.clickCount == 1)
                    {
                        switch (listbox.SelectionMode)
                        {
                            case SelectionMode.Single:
                                listbox.SetSelected(i, true);
                                break;

                            case SelectionMode.Multiple:
                                break;

                            case SelectionMode.Extended:
                                if (!current.shift && !current.control)
                                {
                                    listbox.SetSelected(i, true);
                                    break;
                                }

                                if (!current.shift && current.control)
                                {
                                    listbox.SetSelected(i, !isSelected);
                                    break;
                                }

                                if (current.shift && !current.control)
                                {
                                    //var min = selectedIndexs.Min();
                                    //listbox.SetSelected(i, true);
                                    break;
                                }

                                break;
                        }

                        current.Use();
                    }
                    // check if changed from MouseUp to MouseDown
                    else if (hover && doubleClickCallback != null && current.type == EventType.MouseDown && current.clickCount == 2)
                    {
                        // Debug.Log("Works !");
                        doubleClickCallback(i);
                        current.Use();
                    }
                    else if (current.type == EventType.repaint)
                    {
                        elementStyle.Draw(elementRect, guiContent, hover, false, isSelected, false);
                    }

                    prevElementRect = elementRect;
                }
            }
        }
    }
#endif
}