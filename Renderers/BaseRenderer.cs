﻿//#define UICDEBUG
namespace Codefarts.UIControls.Renderers
{
    using Codefarts.UIControls.Interfaces;
    using System;
    using System.Threading;
    using UnityEngine;

    /// <summary>
    /// Provides a base implementation of the <see cref="IControlRenderer"/> interface.
    /// </summary>
    public abstract class BaseRenderer : IControlRenderer
    {
        /// <summary>
        /// The cached mouse arguments to prevent additional garbage collection.
        /// </summary>
        private MouseEventArgs mouseArguments;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRenderer"/> class.
        /// </summary>
        public BaseRenderer()
        {
            this.mouseArguments = new MouseEventArgs();
        }

        public static int needsRefreshCount = 0;

        public static bool NeedsRefresh
        {
            get
            {
                return needsRefreshCount > 0;
            }
        }

        public static void QueueRefresh()
        {
            Interlocked.Increment(ref needsRefreshCount);
        }

        public static void DropRefresh()
        {
            var count = needsRefreshCount;
            if (count > 0)
            {
                Interlocked.Decrement(ref needsRefreshCount);
            }
        }

        /// <summary>
        /// Draws the specified control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        /// <exception cref="System.ArgumentNullException">control</exception>
        /// <exception cref="System.InvalidCastException">If the type of control does not match the <see cref="ControlType"/> property.</exception>
        /// <remarks>This base renderer implementation will handle visibility and enabled state management, as well as throw exceptions if 
        /// the provided control is null or not of the expected type.</remarks>
        public void Draw(ControlRenderingArgs args)
        {
            if (args == null)
            {
                throw new ArgumentNullException("args");
            }

            var control = args.Control;
            if (control == null)
            {
                throw new ArgumentNullException("args.Control");
            }

            if (!control.IsVisible)
            {
                return;
            }

            var rect = new Rect(control.Left, control.Top, control.Width, control.Height);
            rect.xMax -= control.Width == 0 ? control.Right + control.Left : 0;
            rect.yMax -= control.Height == 0 ? control.Bottom + control.Top : 0;
            if (rect.width < 0 || rect.height < 0)
            {
                return;
            }

            var prevState = GUI.enabled;
            GUI.enabled = prevState && control.IsEnabled;
            //  GUI.depth--;
            this.DrawControl(args);
            GUI.enabled = prevState;
            BaseRenderer.QueueRefresh();
        }


#if UICDEBUG
        private GUIStyle debugStyle;

        protected virtual void DrawDebugFrames(Rect rect, Control control)
        {
            var current = Event.current;
            if (current.type == EventType.Repaint)
            {
                if (this.debugStyle == null)
                {
                    this.debugStyle = new GUIStyle(GUI.skin.label);
                    this.debugStyle.alignment = TextAnchor.MiddleCenter;
                    this.debugStyle.normal.textColor = UnityEngine.Color.white;
                }

                //  var origColor = GUI.color;
                // GUI.color = new Color(0, 1, 0, 0.1f);
                // var whiteTexture = Texture2D.whiteTexture;
                //GUIHelpers.Draw(whiteTexture, rect.x, rect.y, rect.width, rect.height, 0, 0, whiteTexture.width, whiteTexture.height, false, false, true);
                var randomColors = new[] { Color.FromArgb(16, 0, 255, 0) };//, Color.cyan, Color.magenta, Color.red, Color.yellow };
                var rnd = new Random((int)DateTime.Now.Ticks);
                var color = randomColors[rnd.Next(0, randomColors.Length - 1)];
                GUIHelpers.FillRectangle(rect, color);
                //var guiContent = new GUIContent((string.IsNullOrEmpty(control.Name) ? control.GetType().Name : control.Name) + "\r\n" + rect);
                GUIHelpers.DrawRectangle(rect, Colors.White);
                //  GUI.color = origColor;
            }

            //var win = EditorWindow.GetWindow<DebugWindow>();
            //win.Data = this.GetControlHeiarchyData(control) + "\r\nMouse: " + Event.current.mousePosition;
            //win.Repaint();
        }

        private string GetControlHeiarchyData(Control control)
        {
            var data = string.Empty;
            var mPos = Event.current.mousePosition;
            //  mPos.y = -mPos.y;
            var mousePosition = control.PointToScreen(mPos);
            Point point;
            while (control != null)
            {
                point = control.PointToClient(mousePosition);
                if (point.X >= control.Left && point.Y >= control.Top && point.X < control.Left + control.Width && point.Y < control.Top + control.Height)
                {
                    data += string.Format("{0} Location:{1} Size:{2}\r\n", control, control.Location, control.Size);
                }

                control = control.Parent;
            }

            return data;
        }
#endif

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        public virtual void DrawControl(ControlRenderingArgs args)
        {
#if UICDEBUG
            var control = args.Control;
            var rect = new Rect(control.Left + args.Offset.X, control.Top + args.Offset.Y, control.Width, control.Height);
            if (rect.width <= 0 || rect.height <= 0)
            {
                return;
            }

            this.DrawDebugFrames(rect, control);
#endif
        }

        /// <summary>
        /// Updates the specified control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        /// <remarks>
        /// <p>This method is provided if a control has is animated and it's animation state can be updated independently of drawing.</p>
        /// <p>Updates generally occur more frequently then draws.</p>
        /// <p>This base renderer implementation will throw exceptions if the provided control is null or not of the expected type. </p>
        /// </remarks>
        public virtual void Update(ControlRenderingArgs args)
        {
            if (args == null)
            {
                throw new ArgumentNullException("args");
            }

            var controls = args.Control.Controls;
            if (controls == null)
            {
                return;
            }

            foreach (var item in controls)
            {
                args.Manager.UpdateControl(item, Time.deltaTime, Time.time);
            }
        }

        protected void HandleKeyEventsAfterControlDrawn(string controlName, KeyCode keyCode, bool isDown, Control control, bool isUp)
        {
            if (GUI.GetNameOfFocusedControl() == controlName && keyCode != KeyCode.None)
            {
                if (isDown || isUp)
                {
                    control.OnKeyEvent(new KeyEventArgs(keyCode, isDown, isUp));
                }
            }
        }

        protected void GetKeyInfoAndSetControlName(Control control, out KeyCode keyCode, out bool isDown, out bool isUp, out string controlName)
        {
            this.GetKeyInfoAndSetControlName(control, out keyCode, out isDown, out isUp, out controlName, true);
        }

        protected void GetKeyInfoAndSetControlName(Control control, out KeyCode keyCode, out bool isDown, out bool isUp, out string controlName, bool setControlName)
        {
            var current = Event.current;
            keyCode = KeyCode.None;
            isDown = false;
            isUp = false;
            if (control.IsEnabled)
            {
                if (current.isKey && current.keyCode != KeyCode.None && (current.type == EventType.KeyDown || current.type == EventType.KeyUp))
                {
                    keyCode = current.keyCode;
                    isDown = current.type == EventType.KeyDown;
                    isUp = current.type == EventType.KeyUp;
                }
            }

            object objectRef = null;
            controlName = null;
            if (control.Properties != null && control.Properties.TryGetValue(ControlDrawingHelpers.ControlName, out objectRef) && objectRef != null)
            {
                controlName = (string)objectRef;
            }

            if (string.IsNullOrEmpty(controlName))
            {
                controlName = Guid.NewGuid().ToString();
                control.Properties[ControlDrawingHelpers.ControlName] = controlName;
            }

            if (setControlName)
            {
                GUI.SetNextControlName(controlName);
            }
        }



        /*
        /// <summary>
        /// Handles the <see cref="Control.MouseEnter"/> & <see cref="Control.MouseLeave"/> events.
        /// </summary>
        /// <param name="control">The control to check.</param>
        protected void HandleMouseEventsOld(Control control)
        {
            var current = Event.current;
            if (current.type == EventType.Repaint)
            {
                var leftButton = Input.GetMouseButtonDown(0);
                var rightButton = Input.GetMouseButtonDown(1);
                var middleButton = Input.GetMouseButtonDown(2);
                var button1 = Input.GetMouseButtonDown(3);
                var button2 = Input.GetMouseButtonDown(4);

                var rect = new Rect(control.Location, control.Size);
                var relativeMousePosition = current.mousePosition - rect.min;
                var isOver = rect.Contains(current.mousePosition);
                var isMouseOver = control.IsMouseOver;
                var props = control.Properties;

                props[Control.IsMouseOverKey] = isOver;
                var previousMousePosition = new Vector2(int.MinValue, int.MinValue);
                if (props.ContainsKey(Control.PreviousMousePositionKey))
                {
                    previousMousePosition = (Vector2)props[Control.PreviousMousePositionKey];
                }

                if (!isMouseOver && isOver)
                {
                    this.mouseArguments.LeftButton = leftButton;
                    this.mouseArguments.RightButton = rightButton;
                    this.mouseArguments.MiddleButton = middleButton;
                    this.mouseArguments.XButton1 = button1;
                    this.mouseArguments.XButton2 = button2;
                    this.mouseArguments.X = relativeMousePosition.x;
                    this.mouseArguments.Y = relativeMousePosition.y;
                    control.OnMouseEnter(this.mouseArguments);
                }

                if (isOver && previousMousePosition != current.mousePosition)
                {
                    this.mouseArguments.LeftButton = leftButton;
                    this.mouseArguments.RightButton = rightButton;
                    this.mouseArguments.MiddleButton = middleButton;
                    this.mouseArguments.XButton1 = button1;
                    this.mouseArguments.XButton2 = button2;
                    this.mouseArguments.X = relativeMousePosition.x;
                    this.mouseArguments.Y = relativeMousePosition.y;
                    control.OnMouseMove(this.mouseArguments);
                    props[Control.PreviousMousePositionKey] = current.mousePosition;
                }

                if (isMouseOver && !isOver)
                {
                    this.mouseArguments.LeftButton = leftButton;
                    this.mouseArguments.RightButton = rightButton;
                    this.mouseArguments.MiddleButton = middleButton;
                    this.mouseArguments.XButton1 = button1;
                    this.mouseArguments.XButton2 = button2;
                    this.mouseArguments.X = relativeMousePosition.x;
                    this.mouseArguments.Y = relativeMousePosition.y;
                    control.OnMouseLeave(this.mouseArguments);
                }
            }
        }      */
    }
}