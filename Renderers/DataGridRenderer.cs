namespace Codefarts.UIControls.Renderers
{
    using System;

    using Codefarts.UIControls.Controls;

    [ControlRenderer(typeof(DataGrid))]
    public class DataGridRenderer : CommonGridRenderer
    {
        protected override float GetRowHeight(Grid grid, RowDefinition row)
        {
            //if (row.MinHeight > 0)
            //{
            //    return row.Height, row.MaxHeight);
            //}

            //if (row.MaxHeight > 0)
            //{
            //    return Math.Min(row.Height, row.MaxHeight);
            //}
            var dataGrid = (DataGrid)grid;
            var rowHeight = row.Height;
            var dataGridRowHeight = dataGrid.RowHeight;
            var preferedHeight = float.IsNaN(dataGridRowHeight) ? 0 : dataGridRowHeight;

            if (float.IsNaN(rowHeight) || rowHeight < float.Epsilon)
            {
                var rowMaxHeight = row.MaxHeight;
                if (float.IsNaN(rowMaxHeight) || rowMaxHeight < float.Epsilon)
                {
                    return preferedHeight;
                }

                return Math.Min(preferedHeight, rowMaxHeight);
            }

            return rowHeight;
        }

        //protected override float GetMaxRowHeight(Grid grid, RowDefinition row)
        //{
        //    var dataGrid = (DataGrid)grid;
        //    float preferedHeight;
        //    if (dataGrid.RowHeight == float.NaN)
        //    {
        //        preferedHeight = 0;
        //    }
        //    else
        //    {
        //        preferedHeight = dataGrid.RowHeight;
        //    }

        //    var rowMaxHeight = row.MaxHeight;
        //    if (float.IsNaN(rowMaxHeight) || rowMaxHeight < float.Epsilon)
        //    {
        //        return preferedHeight;
        //    }

        //    return row.MaxHeight > 0 ? row.MaxHeight : row.Height;
        //}
    }
}