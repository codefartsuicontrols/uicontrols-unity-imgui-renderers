﻿namespace Codefarts.UIControls.Renderers
{
    using Codefarts.UIControls.Unity;

    using UnityEngine;

    /// <summary>
    /// Provides a renderer implementation for the <see cref="FlexibleSpace"/> control.
    /// </summary>
    public class FlexibleSpaceRenderer : BaseRenderer
    {                     
        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        public override void DrawControl(ControlRenderingArgs args)
        {
            GUILayout.FlexibleSpace();
        }
    }
}