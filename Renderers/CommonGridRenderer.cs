namespace Codefarts.UIControls.Renderers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using UnityEngine;

    public class CommonGridRenderer : BaseRenderer
    {
        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        public override void DrawControl(ControlRenderingArgs args)
        {
            var control = args.Control;
            var grid = control as Grid;
            if (grid == null)
            {
                throw new InvalidCastException("Expected Grid control.");
            }

            var rect = new Rect(control.Left, control.Top, control.Width, control.Height);
            rect.xMax -= control.Width == 0 ? control.Right + control.Left : 0;
            rect.yMax -= control.Height == 0 ? control.Bottom + control.Top : 0;

            // draw background
            var brush = grid.Background;
            if (brush != null)
            {
                BrushExtensions.Draw(brush, rect);
            }

            var columnDefinitions = grid.ColumnDefinitions.Where(x => x.IsVisible).ToArray();
            var rowDefinitions = grid.RowDefinitions.Where(x => x.IsVisible).ToArray();

            var totalFixedColumnWidth = columnDefinitions.Sum(x => x.Width);
            var totalFixedRowHeight = rowDefinitions.Sum(x => this.GetRowHeight(grid, x));
            var totalFlexibleColumnWidth = rect.width - totalFixedColumnWidth;
            var totalFlexibleRowHeight = rect.height - totalFixedRowHeight;

            var flexibleColumnCount = columnDefinitions.Sum(x => Math.Abs(x.Width) < float.Epsilon ? 1 : 0);
            var flexibleRowCount = rowDefinitions.Sum(x => Math.Abs(this.GetRowHeight(grid, x)) < float.Epsilon ? 1 : 0);
            var fixedColumnCount = columnDefinitions.Length - flexibleColumnCount;
            var fixedRowCount = rowDefinitions.Length - flexibleRowCount;
            var totalFlexibleColumnWidthPerCell = totalFlexibleColumnWidth / flexibleColumnCount;
            var totalFlexibleRowHeightPerCell = totalFlexibleRowHeight / flexibleRowCount;
            var totalFixedColumnWidthPerCell = totalFixedColumnWidth / fixedColumnCount;
            var totalFixedRowHeightPerCell = totalFixedRowHeight / fixedRowCount;

            // build dictionary of child controls for better performance
            var cellData = new Dictionary<Point, Control>();
            foreach (var child in grid.Controls)
            {
                cellData[child.GetGridPosition()] = child;
            }

            using (new GUI.GroupScope(rect))
            {
                var position = Vector2.zero;
                for (var column = 0; column < columnDefinitions.Length; column++)
                {
                    var columnDefinition = columnDefinitions[column];
                    var columnWidth = Math.Abs(columnDefinition.Width) > float.Epsilon ? columnDefinition.Width : totalFlexibleColumnWidthPerCell;

                    for (var row = 0; row < rowDefinitions.Length; row++)
                    {
                        var rowDefinition = rowDefinitions[row];
                        var height = this.GetRowHeight(grid, rowDefinition);
                        var rowHeight = Math.Abs(height) > float.Epsilon ? height : totalFlexibleRowHeightPerCell;
                        var cellRect = new Rect(position, new Vector2(columnWidth, rowHeight));
                        GUI.DrawTexture(cellRect, Texture2D.whiteTexture);
                        var ctrl = cellData[new Point(column, row)];
                        if (ctrl != null)
                        {
                            ctrl.Bounds = cellRect;
                            args.Manager.DrawControl(ctrl, args.ElapsedGameTime, args.TotalGameTime);
                        }

                        position.y += rowHeight;
                    }

                    position.y = 0;
                    position.x += columnWidth;
                }

                base.DrawControl(args);
            }
        }

        protected virtual float GetRowHeight(Grid grid, RowDefinition row)
        {
            return double.IsNaN(row.Height) || row.Height < float.Epsilon ? 0 : row.Height;
        }

        //protected virtual float GetMaxRowHeight(Grid grid, RowDefinition row)
        //{
        //    return row.MaxHeight > 0 ? row.MaxHeight : row.Height;
        //}
    }
}