﻿namespace Codefarts.UIControls.Renderers
{
    using System.Collections.Generic;

    using Codefarts.UIControls.Interfaces;

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IControlRenderer"/> implementation for the <see cref="TreeView"/> control.
    /// </summary>
    public class TreeViewRenderer : BaseRenderer
    {
        /// <summary>
        /// Caches the selection style.
        /// </summary>
        private static GUIStyle selectionStyle;

        /// <summary>
        /// Caches a normal style.
        /// </summary>
        private GUIStyle normalStyle;

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeViewRenderer"/> class.
        /// </summary>
        public TreeViewRenderer()
        {
            this.Inset = 8;
        }
                        
        /// <summary>
        /// Gets or sets the inset value that controls how far inset tree nodes are drawn.
        /// </summary> 
        /// <remarks>The default value is 8.</remarks>
        public float Inset { get; set; }

        /// <summary>
        /// Implemented by inheritors to draw the actual control.
        /// </summary>
        /// <param name="args">The rendering argument information.</param>
        /// <exception cref="System.ArgumentNullException">control</exception>
        public override void DrawControl(ControlRenderingArgs args)
        {
            var control = args.Control;
            var tree = (TreeView)control;
            GUI.enabled = GUI.enabled && control.IsEnabled;
            if (selectionStyle == null)
            {
                selectionStyle = new GUIStyle("label");
                selectionStyle.name = "TreeViewSelection";
            }

            var stack = new Stack<TreeNode>();
            if (tree.Nodes != null && tree.Nodes.Count > 0)
            {
                try
                {
                    var scroll = GUILayout.BeginScrollView(new Vector2(tree.HorizontialOffset, tree.VerticalOffset), true, true, ControlDrawingHelpers.StandardDimentionOptions(tree));
                    tree.HorizontialOffset = scroll.x;
                    tree.VerticalOffset = scroll.y;
                    foreach (var node in tree.Nodes)
                    {
                        this.DrawNode(tree, args, stack, node);
                    }
                }
                finally
                {
                    GUILayout.EndScrollView();
                }
            }

            // handle mouse enter & leave events
          //  this.HandleMouseEvents(control, args.Offset);
        }

        private void DrawNode(TreeView tree, ControlRenderingArgs args, Stack<TreeNode> stack, TreeNode node)
        {
            if (!node.IsVisible)
            {
                return;
            }

            if (selectionStyle == null)
            {
                selectionStyle = new GUIStyle(GUI.skin.label);
                selectionStyle.normal.textColor = GUI.skin.settings.selectionColor;
                selectionStyle.wordWrap = false;
            }

            if (this.normalStyle != GUI.skin.label)
            {
                this.normalStyle = new GUIStyle(GUI.skin.label) { wordWrap = false };
            }

            var nodeList = node.Nodes;
            try
            {
                GUI.enabled = tree.IsEnabled;
                GUILayout.BeginHorizontal();

                GUILayout.Space(stack.Count * (node.Nodes != null && node.Nodes.Count == 0 ? this.Inset * 2 : this.Inset));
                stack.Push(node);

                if (nodeList != null && nodeList.Count > 0 && GUILayout.Button(node.IsExpanded ? "-" : "+", this.normalStyle))
                {
                    node.IsExpanded = !node.IsExpanded;
                }

                var value = node.Tag as Control;
                if (value != null)
                {
                    args.Manager.DrawControl(value, args.ElapsedGameTime, args.TotalGameTime);
                }
                else
                {
                    var label = tree.SelectedNode == node ? selectionStyle : this.normalStyle;
                    var button = GUILayout.Button(node.Tag.ToString(), label);
                    tree.SelectedNode = button ? node : tree.SelectedNode;
                }

                GUILayout.FlexibleSpace();
            }
            finally
            {
                GUILayout.EndHorizontal();
            }

            if (node.IsExpanded && nodeList != null)
            {
                foreach (var child in nodeList)
                {
                    this.DrawNode(tree, args, stack, child);
                }
            }

            stack.Pop();
        }
    }
}