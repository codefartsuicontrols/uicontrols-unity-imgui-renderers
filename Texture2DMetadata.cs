﻿namespace Codefarts.UIControls.Unity
{
    using System;

    using UnityEngine;

    public class Texture2DMetadata : ImageMetadata
    {
        /// <summary>
        /// Gets the texture mipmap count.
        /// </summary>  
        public int MipmapCount
        {
            get
            {
                return this.texture.mipmapCount;
            }
        }

        /// <summary>
        /// The texture reference.
        /// </summary>
        private Texture2D texture;

        /// <summary>
        /// Initializes a new instance of the <see cref="Texture2DMetadata"/> class.
        /// </summary>
        /// <param name="texture">The texture to retrieve meta data from.</param>
        public Texture2DMetadata(Texture2D texture)
        {
            if (texture == null)
            {
                throw new ArgumentNullException("texture");
            }

            this.texture = texture;
        }
    }
}