namespace Codefarts.UIControls.Unity
{
    using System.IO;

    using UnityEngine;

    /// <summary>
    /// Provides a resource based image source for a unity <see cref="Texture2D"/> type.
    /// </summary>
    public class Texture2DResourceSource : Texture2DSource
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Texture2DSource"/> class.
        /// </summary>
        /// <param name="resourcePath">The path to the <see cref="Texture2D"/> resource.</param>
        public Texture2DResourceSource(string resourcePath)
        {
            var image = Resources.Load<Texture2D>(resourcePath);
            if (image == null)
            {
                throw new FileLoadException("Unable to load Texture2D resource \"" + resourcePath + "\"");
            }

            this.texture = image;
            this.metadata = new Texture2DMetadata(image);
        }
    }
}