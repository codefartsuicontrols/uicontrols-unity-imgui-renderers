﻿namespace Codefarts.UIControls
{
    using System;
    using System.Linq;

    using Codefarts.UIControls.Unity;       

    using UnityEngine;

    public static class BrushExtensions
    {
        public static void Draw(this SolidColorBrush solidBrush, Rect rect)
        {
            if (solidBrush == null)
            {
                return;
            }

            var col = solidBrush.Color;
            var originalColor = GUI.color;
            GUI.color = new Color(col.A * solidBrush.Opacity, col.R, col.G, col.B);
            GUI.DrawTextureWithTexCoords(rect, Texture2D.whiteTexture, new Rect(0, 0, 1, 1), true);
            GUI.color = originalColor;
        }

        public static void Draw(this ImageBrush imageBrush, Rect rect)
        {
            if (imageBrush == null)
            {
                return;
            }

            var source = imageBrush.ImageSource as Texture2DSource;
            if (source != null)
            {
                var texture = source.Texture;
                var tile = (imageBrush.TileMode & TileMode.Tile) == TileMode.Tile;
                var flipVertically = (imageBrush.TileMode & TileMode.FlipY) == TileMode.FlipY;
                var flipHorizontally = (imageBrush.TileMode & TileMode.FlipX) == TileMode.FlipX;
                var clippedWidth = Math.Min(rect.width, texture.width);
                var clippedHeight = Math.Min(rect.height, texture.height);
                rect.width = tile ? rect.width : clippedWidth;
                rect.height = tile ? rect.height : clippedHeight;
                DoDraw(
                    texture,
                    rect.x,
                    rect.y,
                    rect.width,
                    rect.height,
                    0,
                    0,
                    (int)clippedWidth,
                    (int)clippedHeight,
                    flipHorizontally,
                    flipVertically,
                    tile);
            }
        }

        /// <summary>
        /// Draws a image image.
        /// </summary>
        /// <param name="image">The destination image.</param>
        /// <param name="x">The x position in the destination image.</param>
        /// <param name="y">The y position in the destination image.</param>
        /// <param name="width">The destination width of the drawn image.</param>
        /// <param name="height">The destination height of the drawn image.</param>
        /// <param name="sourceX">The x position in the source image.</param>
        /// <param name="sourceY">The y position in the source image.</param>
        /// <param name="sourceWidth">The source width.</param>
        /// <param name="sourceHeight">The source height.</param>
        /// <param name="flipHorizontally">If set to <c>true</c> the image will be drawn fliped horizontally.</param>
        /// <param name="flipVertically">If set to <c>true</c> the image will be drawen flipped vertically.</param>
        /// <param name="tile">If set to <c>true</c> the image will be tiled across the destination area.</param>
        /// <exception cref="System.ArgumentNullException">image</exception>    
        /// <exception cref="ArgumentOutOfRangeException">If width, height, sourceWidth or sourceHeight are less then 1.</exception>
        /// <remarks>This method has issues with Clamped textures.</remarks>
        public static void DoDraw(Texture2D image, float x, float y, float width, float height, float sourceX, float sourceY, float sourceWidth, float sourceHeight,
            bool flipHorizontally, bool flipVertically, bool tile)
        {
            // perform input validation
            if (image == null)
            {
                throw new ArgumentNullException("image");
            }

            if (sourceWidth < float.Epsilon)
            {
                throw new ArgumentOutOfRangeException("sourceWidth");
            }

            if (sourceHeight < float.Epsilon)
            {
                throw new ArgumentOutOfRangeException("sourceHeight");
            }

            if (width < float.Epsilon)
            {
                return;
            }

            if (height < float.Epsilon)
            {
                return;
            }

            var imgWidth = (float)image.width;
            var imgHeight = (float)image.height;
            var srcWidth = sourceWidth / imgWidth;
            var srcHeight = sourceHeight / imgHeight;
            var position = new Rect(x, y + height, width, -height);
            if (tile)
            {
                srcWidth = width / imgWidth;
                srcHeight = height / imgHeight;
            }

            srcWidth = flipHorizontally ? -srcWidth : srcWidth;
            srcHeight = !flipVertically ? -srcHeight : srcHeight;

            var texCoords = new Rect(sourceX / imgWidth, imgHeight - (sourceY / imgHeight), srcWidth, srcHeight);
            //Debug.Log(string.Format("position: {0} texCoords: {1}", position, texCoords));
            GUI.DrawTextureWithTexCoords(position, image, texCoords, true);
        }

        public static void Draw(this GridImageBrush gridBrush, Rect rect)
        {
            if (gridBrush == null)
            {
                return;
            }

            var source = gridBrush.ImageSource as Texture2DSource;
            if (source == null)
            {
                return;
            }

            gridBrush.Draw(rect, new Rect(0, 0, source.Width, source.Height));
        }

        public static void Draw(this GridImageBrush gridBrush, Rect rect, Rect sourceRect)
        {
            if (gridBrush == null)
            {
                return;
            }

            var source = gridBrush.ImageSource as Texture2DSource;
            if (source == null)
            {
                return;
            }

            // Using the arguments the Draw Method should determine whitch one of the Normal, Focused, Hover, Active, Inactive rect properties to use when
            // drawing the brush.
            var texture = source.Texture;

            var columnDefinitions = gridBrush.ColumnDefinitions.Where(x => x.IsVisible).ToArray();
            var rowDefinitions = gridBrush.RowDefinitions.Where(x => x.IsVisible).ToArray();

            var totalFixedColumnWidth = columnDefinitions.Sum(x => x.Width);
            var totalFixedRowHeight = rowDefinitions.Sum(x => x.Height);
            var totalFlexibleColumnWidth = rect.width - totalFixedColumnWidth;
            var totalFlexibleRowHeight = rect.height - totalFixedRowHeight;
             
            var totalDestFixedColumnWidth = gridBrush.ColumnDefinitions.Sum(x => x.Width);
            var totalDestFixedRowHeight = gridBrush.RowDefinitions.Sum(x => x.Height);
            var totalDestFlexibleColumnWidth = rect.width - totalFixedColumnWidth;
            var totalDestFlexibleRowHeight = rect.height - totalFixedRowHeight;

            var flexibleColumnCount = columnDefinitions.Sum(x => Math.Abs(x.Width) < float.Epsilon ? 1 : 0);
            var flexibleRowCount = rowDefinitions.Sum(x => Math.Abs(x.Height) < float.Epsilon ? 1 : 0);
            var fixedColumnCount = columnDefinitions.Length - flexibleColumnCount;
            var fixedRowCount = rowDefinitions.Length - flexibleRowCount;
            var totalFlexibleColumnWidthPerCell = totalFlexibleColumnWidth / flexibleColumnCount;
            var totalFlexibleRowHeightPerCell = totalFlexibleRowHeight / flexibleRowCount;
            var totalFixedColumnWidthPerCell = totalFixedColumnWidth / fixedColumnCount;
            var totalFixedRowHeightPerCell = totalFixedRowHeight / fixedRowCount;
            
            var totalDestFlexibleColumnWidthPerCell = totalDestFlexibleColumnWidth / flexibleColumnCount;
            var totalDestFlexibleRowHeightPerCell = totalDestFlexibleRowHeight / flexibleRowCount;

            var position = Vector2.zero;
            var drawPosition = rect.position;
          
            //  var text = string.Empty;
            for (var column = 0; column < columnDefinitions.Length; column++)
            {
                var columnDefinition = columnDefinitions[column];
                var columnWidth = Math.Abs(columnDefinition.Width) > float.Epsilon ? columnDefinition.Width : totalFlexibleColumnWidthPerCell;
                var columnDestWidth = Math.Abs(columnDefinition.Width) > float.Epsilon ? columnDefinition.Width : totalDestFlexibleColumnWidthPerCell;

                for (var row = 0; row < rowDefinitions.Length; row++)
                {
                    var rowDefinition = rowDefinitions[row];
                    var rowHeight = Math.Abs(rowDefinition.Height) > float.Epsilon ? rowDefinition.Height : totalFlexibleRowHeightPerCell;
                    var rowDestHeight = Math.Abs(rowDefinition.Height) > float.Epsilon ? rowDefinition.Height : totalDestFlexibleRowHeightPerCell;
                    var cellRect = new Rect(position, new Vector2(columnWidth, rowHeight));

                    var drawSize = new Vector2(columnDestWidth, rowDestHeight);

                    DoDraw(
                        texture,
                        drawPosition.x,
                        drawPosition.y,
                        drawSize.x,
                        drawSize.y,
                        (cellRect.x + sourceRect.x),
                        (cellRect.y + sourceRect.y),
                        cellRect.width,
                        cellRect.height,
                        false,
                        false,
                        false);

                    // text += string.Format("Row: {0} Column: {1} cellRect: {2} - drawRect: {3} - rect: {4}\r\n", row, column, cellRect, new Rect(drawPosition, drawSize), rect);
                    drawPosition.y += drawSize.y;
                    position.y += rowHeight;
                }

                drawPosition.x += columnDestWidth;// columnDefinition.Width == 0 ? (columnWidth / sourceRect.width) * rect.width : columnWidth;
                drawPosition.y = rect.y;
                position.x += columnWidth;
                position.y = 0;
            }

            //  Debug.Log(text);
        }

        public static void Draw(this SkinningBrush skinningBrush, Rect rect, bool focused, bool inactive, bool active, bool hover)
        {
            if (skinningBrush == null)
            {
                return;
            }

            var source = skinningBrush.ImageSource as Texture2DSource;
            if (source == null)
            {
                return;
            }

            // Using the arguments the Draw Method should determine whitch one of the Normal, Focused, Hover, Active, Inactive rect properties to use when
            // drawing the brush.

            var gridBrush = skinningBrush as GridImageBrush;

            if (focused) // focused state
            {
                Draw(gridBrush, rect, skinningBrush.Focused);
                return;
            }

            if (!focused && !inactive && !active && !hover) // normal state
            {
                Draw(gridBrush, rect, skinningBrush.Normal);
                return;
            }

            if (hover) // hover state
            {
                Draw(gridBrush, rect, skinningBrush.Hover);
                return;
            }

            if (active) // active state
            {
                Draw(gridBrush, rect, skinningBrush.Active);
                return;
            }

            if (inactive) // inactiuve state
            {
                Draw(gridBrush, rect, skinningBrush.Inactive);
            }
        }

        public static void Draw(this Brush brush, Rect rect)
        {
            var solidBrush = brush as SolidColorBrush;
            if (solidBrush != null)
            {
                solidBrush.Draw(rect);
                return;
            }

            var imageBrush = brush as ImageBrush;
            if (imageBrush != null)
            {
                imageBrush.Draw(rect);
            }
        }
    }
}
